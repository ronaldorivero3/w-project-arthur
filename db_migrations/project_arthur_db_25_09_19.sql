-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-09-2019 a las 06:55:46
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.1.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `project_arthur_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `answers`
--

CREATE TABLE `answers` (
  `id` int(10) UNSIGNED NOT NULL,
  `answer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_correct` tinyint(1) NOT NULL,
  `question_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `answers`
--

INSERT INTO `answers` (`id`, `answer`, `is_correct`, `question_id`, `created_at`, `updated_at`) VALUES
(18, 'El huevo', 1, 9, '2019-08-26 05:05:20', '2019-09-02 06:56:58'),
(19, 'La gallina', 0, 9, '2019-08-26 05:05:40', '2019-08-26 05:05:40'),
(20, 'Mi super respuesta', 1, 11, '2019-09-02 22:59:24', '2019-09-02 22:59:24'),
(21, '2', 0, 10, '2019-09-21 01:21:54', '2019-09-21 01:21:54'),
(22, '4', 1, 10, '2019-09-21 01:22:03', '2019-09-21 01:22:03'),
(23, '3', 0, 10, '2019-09-21 01:22:11', '2019-09-21 01:22:11'),
(24, 'N/A', 0, 10, '2019-09-21 01:22:40', '2019-09-21 01:22:40'),
(25, '5', 1, 12, '2019-09-21 01:27:53', '2019-09-21 01:27:53'),
(26, 'x^6', 1, 13, '2019-09-21 01:30:43', '2019-09-21 01:30:43'),
(27, '11', 1, 14, '2019-09-21 01:32:44', '2019-09-21 01:32:44'),
(28, '6x', 1, 15, '2019-09-21 01:33:19', '2019-09-21 01:33:19'),
(29, '144', 1, 16, '2019-09-21 01:34:02', '2019-09-21 01:34:02'),
(30, '6', 1, 17, '2019-09-21 01:56:03', '2019-09-21 01:56:03'),
(31, 'y=0', 0, 18, '2019-09-21 01:56:59', '2019-09-21 01:56:59'),
(32, 'y=-x', 1, 18, '2019-09-21 01:57:16', '2019-09-21 01:57:16'),
(33, '3', 1, 19, '2019-09-21 01:58:03', '2019-09-21 01:58:03'),
(34, '30', 1, 20, '2019-09-21 01:58:43', '2019-09-21 01:58:43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `areas`
--

CREATE TABLE `areas` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `banner` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'img/areas/default_banner.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `areas`
--

INSERT INTO `areas` (`id`, `name`, `description`, `created_at`, `updated_at`, `banner`) VALUES
(3, '501', 'Ciencias de la computacion', '2019-08-13 07:16:30', '2019-09-19 18:14:04', 'areas\\September2019\\4EbED7AV0752SgMmbfv0.jpg'),
(4, '100', 'Area 100 Tecnologia y Politecnica', '2019-08-20 05:17:57', '2019-08-20 05:17:57', 'areas\\August2019\\VsZENoZNeBlu8tFA3Us7.jpg'),
(5, '200', 'Salud, Veterinaria, Farmacia y Bioquimica', '2019-08-20 05:19:18', '2019-08-20 05:19:18', 'areas\\August2019\\sr19Ci1UhKoGb0F3hHzF.jpg'),
(6, '300', 'Economia, Finanzas, Auditoria, Administracion, Diseño & Arte', '2019-08-20 05:20:15', '2019-08-20 05:20:15', 'areas\\August2019\\PnFLBBGMvDj81YLEkvQa.jpg'),
(7, '400', 'Humanidades & Ciencias Juridicas', '2019-08-20 05:21:02', '2019-08-20 05:21:02', 'areas\\August2019\\4oW9j4IfNF2vFV1jhjIm.png'),
(8, '600', 'Actividad Fisica', '2019-08-20 05:22:02', '2019-08-20 05:22:02', 'areas\\August2019\\c1FU8I6uThPDKAqLKLfj.jpg'),
(9, '800', 'Ciencias agricolas', '2019-08-20 05:23:42', '2019-08-20 05:23:42', 'areas\\August2019\\ysySPtGgXldnKo4BABrf.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `courses`
--

CREATE TABLE `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `banner` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'img/courses/default_banner.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `courses`
--

INSERT INTO `courses` (`id`, `name`, `description`, `created_at`, `updated_at`, `banner`) VALUES
(1, 'Matematicas', 'Curso de matematicas', '2019-08-13 07:03:00', '2019-09-19 18:12:00', 'courses\\September2019\\ARNctyLpnm0JPGO28LXk.jpg'),
(2, 'Quimica', 'Curso de quimica', '2019-08-20 06:15:00', '2019-09-19 18:11:48', 'courses\\September2019\\DUHqu9sxwqeKa2vpCKY3.jpg'),
(3, 'Fisica', 'Curso de fisica', '2019-08-20 06:15:29', '2019-09-19 18:11:37', 'courses\\September2019\\ZCP7b2POv4LIWvHGWuDb.jpg'),
(4, 'Lenguaje', 'Curso de lenguaje', '2019-08-20 06:17:31', '2019-09-19 18:11:16', 'courses\\September2019\\6kKXYdBcayJKNfsYacKh.jpg'),
(5, 'Biologia', 'Curso de biologia', '2019-08-20 06:17:58', '2019-09-19 18:10:47', 'courses\\September2019\\Y9xOUC3RwBJb7Vz60aBX.jpg'),
(6, 'Psicologia', 'Curso de psicologia', '2019-08-20 06:18:47', '2019-09-19 18:10:39', 'courses\\September2019\\ITbXicst7fE9ixLrDu9y.jpg'),
(7, 'Historia', 'Curso de historia', '2019-08-20 06:19:09', '2019-09-19 18:10:26', 'courses\\September2019\\XyE3vtkkWvntL3BCCeYb.jpg'),
(8, 'Filosofia', 'Curso de filosofia', '2019-08-20 06:19:29', '2019-09-19 18:10:17', 'courses\\September2019\\5qVdIwXMqXm53B6Yn6jZ.jpg'),
(9, 'Ingles', 'Curso de Ingles', '2019-08-20 06:19:55', '2019-09-19 18:10:06', 'courses\\September2019\\rtAsyEtsc6cZBgWVbpcE.jpg'),
(10, 'Computacion', 'Curso de computacion', '2019-08-20 06:20:10', '2019-09-19 18:09:54', 'courses\\September2019\\rnPZkHK1YSu7kFyxW8gt.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `courses_areas`
--

CREATE TABLE `courses_areas` (
  `id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `area_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `courses_areas`
--

INSERT INTO `courses_areas` (`id`, `course_id`, `area_id`) VALUES
(3, 1, 3),
(4, 1, 5),
(5, 1, 6),
(6, 1, 9),
(7, 2, 4),
(8, 2, 5),
(9, 2, 9),
(10, 3, 3),
(11, 3, 4),
(12, 4, 4),
(13, 4, 5),
(14, 4, 6),
(15, 4, 7),
(16, 4, 8),
(17, 4, 9),
(18, 5, 5),
(19, 5, 8),
(20, 6, 6),
(21, 6, 7),
(22, 6, 8),
(23, 7, 6),
(24, 7, 7),
(25, 7, 8),
(26, 8, 7),
(27, 9, 3),
(28, 10, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(2, 1, 'name', 'text', 'Nombre', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"El nombre del usuario es obligatorio\"}}}', 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"El email del usuario es obligatorio\"}}}', 3),
(4, 1, 'password', 'password', 'Contraseña', 1, 0, 0, 1, 1, 0, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"La contrase\\u00f1a del usuario es obligatoria\"}}}', 5),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, '{}', 6),
(6, 1, 'created_at', 'timestamp', 'Creado en', 0, 1, 1, 0, 0, 0, '{}', 4),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, '{\"default\":\"users\\/default-student.png\"}', 13),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Rol', 0, 1, 1, 1, 1, 1, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 15),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 0, 0, 0, 0, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 16),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, '{}', 17),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Rol', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"El rol del usuario es obligatorio\"}}}', 14),
(22, 4, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 4, 'answer', 'text', 'Respuesta', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"La respuesta es obligatoria\"}}}', 3),
(24, 4, 'is_correct', 'checkbox', 'Es correcta', 1, 1, 1, 1, 1, 1, '{\"default\":0,\"description\":\"Valor que indica si la respuesta introducida sera tratada como correcta\"}', 4),
(25, 4, 'question_id', 'text', 'Question Id', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"La pregunta es obligatoria\"}}}', 2),
(26, 4, 'created_at', 'timestamp', 'Creado en', 0, 1, 1, 0, 0, 0, '{}', 5),
(27, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(28, 5, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(29, 5, 'name', 'text', 'Nombre', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"El nombre del area es obligatorio\"}}}', 2),
(30, 5, 'description', 'text_area', 'Descripcion', 1, 1, 1, 1, 1, 1, '{\"null\":\"\"}', 3),
(31, 5, 'created_at', 'timestamp', 'Creado en', 0, 1, 1, 0, 0, 0, '{}', 4),
(32, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(33, 6, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(34, 6, 'name', 'text', 'Nombre', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"El nombre del curso es obligatorio\"}}}', 2),
(35, 6, 'description', 'text_area', 'Descripcion', 1, 1, 1, 1, 1, 1, '{\"null\":\"\"}', 3),
(38, 6, 'created_at', 'timestamp', 'Creado en', 0, 1, 1, 0, 0, 0, '{}', 6),
(39, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(40, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(41, 8, 'course_id', 'text', 'Course Id', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"El curso es obligatorio\"}}}', 2),
(42, 8, 'area_id', 'text', 'Area Id', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"El area es obligatoria\"}}}', 3),
(43, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(44, 9, 'question', 'text', 'Pregunta', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"La pregunta es obligatoria\"}}}', 3),
(45, 9, 'answers_quantity', 'number', 'Cantidad de respuestas', 1, 1, 1, 0, 0, 0, '{\"description\":\"La cantidad de respuesta que tiene esta pregunta\"}', 4),
(46, 9, 'course_id', 'text', 'Course Id', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"El curso es obligatorio\"}}}', 2),
(47, 9, 'created_at', 'timestamp', 'Creado en', 0, 1, 1, 0, 0, 0, '{}', 5),
(48, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(49, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(50, 11, 'question_id', 'text', 'Question Id', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"La pregunta es obligatoria\"}}}', 2),
(51, 11, 'video_id', 'text', 'Video Id', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"El video es obligatorio\"}}}', 3),
(52, 11, 'answer_link_description', 'text', 'Descripcion', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"La descripcion del link es obligatoria\"}}}', 4),
(53, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(54, 12, 'name', 'text', 'Nombre', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"El nombre del recurso es obligatorio\"}}}', 2),
(55, 12, 'description', 'text_area', 'Descripcion', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"La descripcion del recurso es obligatoria\"}}}', 3),
(57, 12, 'uri', 'file', 'URI', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"El recurso es obligatorio\"}}}', 4),
(58, 12, 'created_at', 'timestamp', 'Creado en', 0, 1, 1, 0, 0, 0, '{}', 6),
(59, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(60, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(61, 13, 'score', 'text', 'Puntuacion', 1, 1, 1, 1, 1, 1, '{}', 3),
(62, 13, 'spent_time', 'text', 'Tiempo ocupado', 1, 1, 1, 1, 1, 1, '{}', 4),
(63, 13, 'status', 'text', 'Estado', 1, 1, 1, 1, 1, 1, '{}', 5),
(64, 13, 'user_id', 'text', 'User Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(65, 13, 'created_at', 'timestamp', 'Creado en', 0, 1, 1, 0, 0, 1, '{}', 6),
(66, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(67, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(68, 14, 'created_at', 'timestamp', 'Creado en', 0, 1, 1, 0, 0, 1, '{}', 5),
(69, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(70, 14, 'question_id', 'text', 'Question Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(71, 14, 'answer_id', 'text', 'Answer Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(72, 14, 'simulator_history_id', 'text', 'Simulator History Id', 1, 1, 1, 1, 1, 1, '{}', 4),
(73, 17, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(74, 17, 'user_id', 'text', 'User Id', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"El usuario es obligatorio\"}}}', 2),
(75, 17, 'area_id', 'text', 'Area Id', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"El area es obligatorio\"}}}', 3),
(76, 18, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(77, 18, 'name', 'text', 'Nombre', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"El nombre del video es obligatorio\"}}}', 3),
(78, 18, 'description', 'text', 'Descripcion', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"La descripcion del video es obligatoria\"}}}', 4),
(80, 18, 'uri', 'file', 'URI', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"El video es obligatorio\"}}}', 6),
(81, 18, 'course_id', 'text', 'Course Id', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"El curso es obligatorio\"}}}', 2),
(82, 18, 'created_at', 'timestamp', 'Creado en', 0, 1, 1, 0, 0, 0, '{}', 7),
(83, 18, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(84, 4, 'answer_belongsto_question_relationship', 'relationship', 'Pregunta', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Question\",\"table\":\"questions\",\"type\":\"belongsTo\",\"column\":\"question_id\",\"key\":\"id\",\"label\":\"question\",\"pivot_table\":\"answers\",\"pivot\":\"0\",\"taggable\":\"0\"}', 7),
(85, 4, 'answer_hasmany_simulator_histories_question_relationship', 'relationship', 'simulator_histories_questions', 0, 0, 0, 0, 0, 0, '{\"model\":\"App\\\\Models\\\\SimulatorHistoryQuestion\",\"table\":\"simulator_histories_questions\",\"type\":\"hasMany\",\"column\":\"answer_id\",\"key\":\"id\",\"label\":\"id\",\"pivot_table\":\"answers\",\"pivot\":\"0\",\"taggable\":\"0\"}', 8),
(86, 5, 'area_belongstomany_course_relationship', 'relationship', 'Cursos', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Course\",\"table\":\"courses\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"courses_areas\",\"pivot\":\"1\",\"taggable\":\"0\"}', 7),
(87, 5, 'area_belongstomany_user_relationship', 'relationship', 'Usuarios', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\User\",\"table\":\"users\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"users_areas\",\"pivot\":\"1\",\"taggable\":\"0\"}', 8),
(88, 6, 'course_belongstomany_area_relationship', 'relationship', 'Areas', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Area\",\"table\":\"areas\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"courses_areas\",\"pivot\":\"1\",\"taggable\":\"0\"}', 9),
(89, 6, 'course_hasmany_video_relationship', 'relationship', 'Videos', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Video\",\"table\":\"videos\",\"type\":\"hasMany\",\"column\":\"course_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"answers\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(90, 6, 'course_hasmany_question_relationship', 'relationship', 'Preguntas', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Question\",\"table\":\"questions\",\"type\":\"hasMany\",\"column\":\"course_id\",\"key\":\"id\",\"label\":\"question\",\"pivot_table\":\"answers\",\"pivot\":\"0\",\"taggable\":\"0\"}', 11),
(91, 8, 'courses_area_belongsto_course_relationship', 'relationship', 'Curso', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Course\",\"table\":\"courses\",\"type\":\"belongsTo\",\"column\":\"course_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"answers\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(92, 8, 'courses_area_belongsto_area_relationship', 'relationship', 'Area', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Area\",\"table\":\"areas\",\"type\":\"belongsTo\",\"column\":\"area_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"answers\",\"pivot\":\"0\",\"taggable\":\"0\"}', 5),
(93, 9, 'question_belongsto_course_relationship', 'relationship', 'Curso', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Course\",\"table\":\"courses\",\"type\":\"belongsTo\",\"column\":\"course_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"answers\",\"pivot\":\"0\",\"taggable\":\"0\"}', 7),
(94, 9, 'question_belongstomany_video_relationship', 'relationship', 'Videos', 0, 0, 0, 0, 0, 0, '{\"model\":\"App\\\\Models\\\\Video\",\"table\":\"videos\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"questions_videos\",\"pivot\":\"1\",\"taggable\":\"0\"}', 8),
(95, 9, 'question_hasone_answer_relationship', 'relationship', 'Respuestas', 0, 0, 0, 0, 0, 0, '{\"model\":\"App\\\\Models\\\\Answer\",\"table\":\"answers\",\"type\":\"hasOne\",\"column\":\"question_id\",\"key\":\"id\",\"label\":\"answer\",\"pivot_table\":\"answers\",\"pivot\":\"0\",\"taggable\":\"0\"}', 9),
(96, 11, 'questions_video_belongsto_question_relationship', 'relationship', 'Pregunta', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Question\",\"table\":\"questions\",\"type\":\"belongsTo\",\"column\":\"question_id\",\"key\":\"id\",\"label\":\"question\",\"pivot_table\":\"answers\",\"pivot\":\"0\",\"taggable\":\"0\"}', 5),
(97, 11, 'questions_video_belongsto_video_relationship', 'relationship', 'Video', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Video\",\"table\":\"videos\",\"type\":\"belongsTo\",\"column\":\"video_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"answers\",\"pivot\":\"0\",\"taggable\":\"0\"}', 6),
(98, 18, 'video_belongsto_course_relationship', 'relationship', 'Cursos', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Course\",\"table\":\"courses\",\"type\":\"belongsTo\",\"column\":\"course_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"answers\",\"pivot\":\"0\",\"taggable\":\"0\"}', 9),
(99, 18, 'video_belongstomany_question_relationship', 'relationship', 'Preguntas', 0, 1, 1, 0, 0, 0, '{\"model\":\"App\\\\Models\\\\Question\",\"table\":\"questions\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"question\",\"pivot_table\":\"questions_videos\",\"pivot\":\"1\",\"taggable\":\"0\"}', 10),
(100, 13, 'simulator_history_belongsto_user_relationship', 'relationship', 'Usuario', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"answers\",\"pivot\":\"0\",\"taggable\":\"0\"}', 8),
(101, 1, 'user_hasmany_simulator_history_relationship', 'relationship', 'Historial de simulador', 0, 0, 0, 0, 0, 0, '{\"model\":\"App\\\\Models\\\\SimulatorHistory\",\"table\":\"simulator_histories\",\"type\":\"hasMany\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"status\",\"pivot_table\":\"answers\",\"pivot\":\"0\",\"taggable\":\"0\"}', 18),
(102, 13, 'simulator_history_belongstomany_question_relationship', 'relationship', 'Preguntas', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Question\",\"table\":\"questions\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"question\",\"pivot_table\":\"simulator_histories_questions\",\"pivot\":\"1\",\"taggable\":\"0\"}', 9),
(103, 9, 'question_belongstomany_simulator_history_relationship', 'relationship', 'HIstorias de simulador', 0, 0, 0, 0, 0, 0, '{\"model\":\"App\\\\Models\\\\SimulatorHistory\",\"table\":\"simulator_histories\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"status\",\"pivot_table\":\"simulator_histories_questions\",\"pivot\":\"1\",\"taggable\":\"0\"}', 10),
(104, 14, 'simulator_histories_question_belongsto_simulator_history_relationship', 'relationship', 'Historial de simulador', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\SimulatorHistory\",\"table\":\"simulator_histories\",\"type\":\"belongsTo\",\"column\":\"simulator_history_id\",\"key\":\"id\",\"label\":\"status\",\"pivot_table\":\"answers\",\"pivot\":\"0\",\"taggable\":\"0\"}', 7),
(105, 14, 'simulator_histories_question_belongsto_question_relationship', 'relationship', 'Pregunta', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Question\",\"table\":\"questions\",\"type\":\"belongsTo\",\"column\":\"question_id\",\"key\":\"id\",\"label\":\"question\",\"pivot_table\":\"answers\",\"pivot\":\"0\",\"taggable\":\"0\"}', 8),
(106, 14, 'simulator_histories_question_belongsto_answer_relationship', 'relationship', 'Respuesta', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Answer\",\"table\":\"answers\",\"type\":\"belongsTo\",\"column\":\"answer_id\",\"key\":\"id\",\"label\":\"answer\",\"pivot_table\":\"answers\",\"pivot\":\"0\",\"taggable\":\"0\"}', 9),
(107, 1, 'user_belongstomany_area_relationship', 'relationship', 'Areas', 0, 0, 0, 0, 0, 0, '{\"model\":\"App\\\\Models\\\\Area\",\"table\":\"areas\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"users_areas\",\"pivot\":\"1\",\"taggable\":\"0\"}', 19),
(108, 17, 'users_area_belongsto_area_relationship', 'relationship', 'Area', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Area\",\"table\":\"areas\",\"type\":\"belongsTo\",\"column\":\"area_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"answers\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(109, 17, 'users_area_belongsto_user_relationship', 'relationship', 'Usuario', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"answers\",\"pivot\":\"0\",\"taggable\":\"0\"}', 5),
(110, 6, 'course_hasmany_resource_relationship', 'relationship', 'Recursos', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Resource\",\"table\":\"resources\",\"type\":\"hasMany\",\"column\":\"course_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"answers\",\"pivot\":\"0\",\"taggable\":\"0\"}', 12),
(111, 12, 'resource_belongsto_course_relationship', 'relationship', 'Cursos', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Course\",\"table\":\"courses\",\"type\":\"belongsTo\",\"column\":\"course_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"answers\",\"pivot\":\"0\",\"taggable\":\"0\"}', 8),
(112, 5, 'banner', 'image', 'Banner', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"El banner del area es obligatorio\"}}}', 6),
(113, 6, 'banner', 'image', 'Banner', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"El banner del curso es obligatorio\"}}}', 8),
(114, 1, 'phone', 'text', 'Celular', 0, 1, 1, 1, 1, 1, '{\"null\":\"\"}', 7),
(115, 1, 'direction', 'text', 'Direccion', 0, 0, 1, 1, 1, 1, '{\"null\":\"\"}', 9),
(116, 1, 'email_verified_at', 'timestamp', 'Email Verified At', 0, 0, 0, 0, 0, 0, '{}', 11),
(117, 1, 'ci', 'text', 'Carnet de identidad', 0, 1, 1, 1, 1, 1, '{\"null\":\"\"}', 8),
(118, 1, 'active', 'checkbox', 'Activo', 1, 1, 1, 1, 1, 1, '{\"default\":0}', 12),
(119, 12, 'course_id', 'text', 'Course Id', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"El curso es obligatorio\"}}}', 5),
(125, 21, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(126, 21, 'key', 'text', 'Key', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"La key del permiso es obligatoria\"}}}', 2),
(127, 21, 'table_name', 'text', 'Nombre de la tabla', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"El nombre de la tabla es obligatoria\"}}}', 3),
(128, 21, 'created_at', 'timestamp', 'Creado en', 0, 1, 1, 0, 0, 0, '{}', 4),
(129, 21, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'Usuario', 'Usuarios', 'voyager-person', 'App\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'App\\Http\\Controllers\\Admin\\UserController', NULL, 1, 1, '{\"order_column\":\"id\",\"order_display_column\":\"name\",\"order_direction\":\"desc\",\"default_search_key\":\"id\",\"scope\":null}', '2019-08-01 04:07:20', '2019-09-03 06:26:32'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(4, 'answers', 'answers', 'Respuesta', 'Respuestas', 'voyager-lightbulb', 'App\\Models\\Answer', NULL, 'App\\Http\\Controllers\\Admin\\AnswerController', NULL, 1, 1, '{\"order_column\":\"id\",\"order_display_column\":\"answer\",\"order_direction\":\"desc\",\"default_search_key\":\"id\",\"scope\":null}', '2019-08-01 04:14:08', '2019-09-02 22:59:07'),
(5, 'areas', 'areas', 'Area', 'Areas', 'voyager-pie-graph', 'App\\Models\\Area', NULL, NULL, NULL, 1, 1, '{\"order_column\":\"id\",\"order_display_column\":\"name\",\"order_direction\":\"desc\",\"default_search_key\":\"id\",\"scope\":null}', '2019-08-01 04:14:16', '2019-09-02 23:01:37'),
(6, 'courses', 'courses', 'Curso', 'Cursos', 'voyager-book', 'App\\Models\\Course', NULL, NULL, NULL, 1, 1, '{\"order_column\":\"id\",\"order_display_column\":\"name\",\"order_direction\":\"desc\",\"default_search_key\":\"id\",\"scope\":null}', '2019-08-01 04:14:22', '2019-09-02 22:53:18'),
(8, 'courses_areas', 'courses-areas', 'Curso & Area', 'Cursos & Areas', 'voyager-window-list', 'App\\Models\\CourseArea', NULL, NULL, NULL, 1, 1, '{\"order_column\":\"id\",\"order_display_column\":\"id\",\"order_direction\":\"desc\",\"default_search_key\":\"id\",\"scope\":null}', '2019-08-01 04:15:51', '2019-09-02 22:53:38'),
(9, 'questions', 'questions', 'Pregunta', 'Preguntas', 'voyager-question', 'App\\Models\\Question', NULL, NULL, NULL, 1, 1, '{\"order_column\":\"id\",\"order_display_column\":\"question\",\"order_direction\":\"asc\",\"default_search_key\":\"id\",\"scope\":null}', '2019-08-01 04:16:02', '2019-09-02 22:55:38'),
(11, 'questions_videos', 'questions-videos', 'Pregunta & Video', 'Preguntas & Videos', 'voyager-params', 'App\\Models\\QuestionVideo', NULL, NULL, NULL, 1, 1, '{\"order_column\":\"id\",\"order_display_column\":\"answer_link_description\",\"order_direction\":\"desc\",\"default_search_key\":\"id\",\"scope\":null}', '2019-08-01 04:16:20', '2019-09-02 22:55:57'),
(12, 'resources', 'resources', 'Recurso', 'Recursos', 'voyager-photos', 'App\\Models\\Resource', NULL, NULL, NULL, 1, 1, '{\"order_column\":\"id\",\"order_display_column\":\"id\",\"order_direction\":\"desc\",\"default_search_key\":\"id\",\"scope\":null}', '2019-08-01 04:16:29', '2019-09-02 22:56:19'),
(13, 'simulator_histories', 'simulator-histories', 'Historial de simulador', 'Historias de simulador', 'voyager-book', 'App\\Models\\SimulatorHistory', NULL, NULL, NULL, 1, 1, '{\"order_column\":\"id\",\"order_display_column\":\"id\",\"order_direction\":\"desc\",\"default_search_key\":\"id\",\"scope\":null}', '2019-08-01 04:16:37', '2019-09-02 22:56:40'),
(14, 'simulator_histories_questions', 'simulator-histories-questions', 'Simulador Pregunta & Respuesta', 'Simulador Preguntas & Respuestas', 'voyager-logbook', 'App\\Models\\SimulatorHistoryQuestion', NULL, NULL, NULL, 1, 1, '{\"order_column\":\"id\",\"order_display_column\":\"id\",\"order_direction\":\"desc\",\"default_search_key\":\"id\",\"scope\":null}', '2019-08-01 04:16:58', '2019-09-02 22:56:54'),
(17, 'users_areas', 'users-areas', 'Usuario & Area', 'Usuarios & Areas', 'voyager-company', 'App\\Models\\UserArea', NULL, NULL, NULL, 1, 1, '{\"order_column\":\"id\",\"order_display_column\":\"id\",\"order_direction\":\"desc\",\"default_search_key\":\"id\",\"scope\":null}', '2019-08-01 04:17:32', '2019-09-02 22:57:22'),
(18, 'videos', 'videos', 'Video', 'Videos', 'voyager-youtube-play', 'App\\Models\\Video', NULL, NULL, NULL, 1, 1, '{\"order_column\":\"id\",\"order_display_column\":\"name\",\"order_direction\":\"desc\",\"default_search_key\":\"id\",\"scope\":null}', '2019-08-01 04:17:41', '2019-09-02 22:57:54'),
(21, 'permissions', 'permissions', 'Permiso', 'Permisos', 'voyager-lock', 'TCG\\Voyager\\Models\\Permission', NULL, NULL, NULL, 1, 1, '{\"order_column\":\"id\",\"order_display_column\":\"key\",\"order_direction\":\"asc\",\"default_search_key\":\"id\",\"scope\":null}', '2019-09-03 06:00:47', '2019-09-03 06:03:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(3, 'student_menu', '2019-08-01 04:48:50', '2019-08-01 04:55:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Inicio', '', '_self', 'voyager-boat', '#000000', NULL, 1, '2019-08-01 04:07:20', '2019-08-20 04:59:50', 'voyager.dashboard', 'null'),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 6, '2019-08-01 04:07:20', '2019-09-03 06:01:52', 'voyager.media.index', NULL),
(3, 1, 'Usuarios', '', '_self', 'voyager-person', '#000000', NULL, 4, '2019-08-01 04:07:20', '2019-09-03 06:01:56', 'voyager.users.index', 'null'),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2019-08-01 04:07:20', '2019-08-01 04:07:20', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 7, '2019-08-01 04:07:20', '2019-09-03 06:01:52', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2019-08-01 04:07:20', '2019-08-20 04:51:16', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2019-08-01 04:07:20', '2019-08-20 04:51:16', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2019-08-01 04:07:20', '2019-08-20 04:51:16', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_blank', 'voyager-bread', '#000000', 5, 4, '2019-08-01 04:07:20', '2019-08-25 03:21:48', 'voyager.bread.index', 'null'),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 8, '2019-08-01 04:07:20', '2019-09-03 06:01:52', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2019-08-01 04:07:20', '2019-08-20 04:51:16', 'voyager.hooks', NULL),
(12, 1, 'Respuestas', '', '_self', 'voyager-lightbulb', '#000000', 30, 2, '2019-08-01 04:14:08', '2019-08-20 05:08:58', 'voyager.answers.index', 'null'),
(13, 1, 'Areas', '', '_self', 'voyager-pie-graph', '#000000', 31, 1, '2019-08-01 04:14:16', '2019-08-20 05:11:48', 'voyager.areas.index', 'null'),
(14, 1, 'Adm. Cursos', '', '_self', 'voyager-book', '#000000', 32, 1, '2019-08-01 04:14:22', '2019-08-20 05:12:47', 'voyager.courses.index', 'null'),
(15, 1, 'Areas & Cursos', '', '_self', 'voyager-window-list', '#000000', 31, 2, '2019-08-01 04:15:51', '2019-08-20 05:12:03', 'voyager.courses-areas.index', 'null'),
(16, 1, 'Preguntas', '', '_self', 'voyager-question', '#000000', 30, 1, '2019-08-01 04:16:02', '2019-08-20 05:08:37', 'voyager.questions.index', 'null'),
(17, 1, 'Preguntas & Videos', '', '_self', 'voyager-params', '#000000', 30, 3, '2019-08-01 04:16:20', '2019-08-20 05:09:15', 'voyager.questions-videos.index', 'null'),
(18, 1, 'Recursos', '', '_self', 'voyager-photos', '#000000', 32, 2, '2019-08-01 04:16:29', '2019-08-20 05:13:22', 'voyager.resources.index', 'null'),
(19, 1, 'Historial de simulador', '', '_self', 'voyager-book', '#000000', 30, 4, '2019-08-01 04:16:37', '2019-08-20 05:09:30', 'voyager.simulator-histories.index', 'null'),
(21, 1, 'Usuarios & Areas', '', '_self', 'voyager-company', '#000000', NULL, 5, '2019-08-01 04:17:32', '2019-09-03 06:01:52', 'voyager.users-areas.index', 'null'),
(22, 1, 'Videos', '', '_self', 'voyager-youtube-play', '#000000', 32, 3, '2019-08-01 04:17:41', '2019-08-20 05:13:02', 'voyager.videos.index', 'null'),
(24, 3, 'Inicio', '/', '_self', NULL, '#000000', NULL, 1, '2019-08-01 04:50:09', '2019-08-13 07:33:30', NULL, ''),
(25, 3, 'Historial', '/historial', '_self', NULL, '#000000', NULL, 2, '2019-08-01 04:57:31', '2019-08-13 06:34:50', NULL, ''),
(26, 3, 'Cuenta', '/cuenta', '_self', NULL, '#000000', NULL, 3, '2019-08-01 04:58:09', '2019-09-03 05:32:03', NULL, ''),
(27, 3, 'Configuracion', '/configuracion', '_self', NULL, '#000000', 29, 1, '2019-08-01 04:58:38', '2019-09-03 05:32:03', NULL, ''),
(30, 1, 'Simulador', '', '_self', 'voyager-laptop', '#000000', NULL, 9, '2019-08-20 04:51:07', '2019-09-03 06:01:52', NULL, ''),
(31, 1, 'Academico', '', '_self', 'voyager-book', '#000000', NULL, 10, '2019-08-20 04:52:01', '2019-09-03 06:01:52', NULL, ''),
(32, 1, 'Cursos', '', '_self', 'voyager-categories', '#000000', NULL, 11, '2019-08-20 04:57:26', '2019-09-03 06:01:52', NULL, ''),
(34, 1, 'Preguntas & Historial', '', '_self', 'voyager-logbook', '#000000', 30, 5, '2019-08-20 05:03:57', '2019-08-26 06:39:19', 'voyager.simulator-histories-questions.index', 'null'),
(36, 1, 'Permissions', '', '_self', 'voyager-lock', NULL, NULL, 3, '2019-09-03 06:00:47', '2019-09-03 06:01:56', 'voyager.permissions.index', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2019_06_22_220823_create_courses_table', 1),
(24, '2019_06_22_220939_create_areas_table', 1),
(25, '2019_06_22_221016_create_resources_table', 1),
(26, '2019_06_22_221030_create_videos_table', 1),
(27, '2019_06_22_221050_create_questions_table', 1),
(28, '2019_06_22_221117_create_answers_table', 1),
(29, '2019_06_22_221146_create_simulator_histories_table', 1),
(30, '2019_06_22_221208_create_user_areas_table', 1),
(31, '2019_06_22_221224_create_course_areas_table', 1),
(32, '2019_06_22_221317_create_questions_videos_table', 1),
(33, '2019_06_22_221336_create_simulator_histories_questions_table', 1),
(34, '2019_08_09_181606_add_banner_column_areas_table', 2),
(35, '2019_08_09_181653_add_banner_column_courses_table', 2),
(36, '2019_08_20_020813_update_courses', 3),
(37, '2019_08_20_021155_update_question_answer_quantity', 3),
(38, '2019_08_24_212324_add_direction_active_ci_phone_users_table', 4),
(39, '2019_09_02_030012_set_nullable_filetype_resource', 5),
(40, '2019_09_02_033027_remove_file_type_field_from_resource', 6),
(41, '2019_09_02_034837_remove_quantity_fields_from_courses', 7),
(42, '2019_09_02_035620_remove_duration_from_videos', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('fabiocm_08@hotmail.com', '$2y$10$7sqs7inJ4LXgN6xPIiJtQuSCV94qqwABdMd6W0AZaxdCuTcRrCfRC', '2019-09-14 21:17:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(2, 'browse_bread', NULL, '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(3, 'browse_database', NULL, '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(4, 'browse_media', NULL, '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(5, 'browse_compass', NULL, '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(6, 'browse_menus', 'menus', '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(7, 'read_menus', 'menus', '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(8, 'edit_menus', 'menus', '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(9, 'add_menus', 'menus', '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(10, 'delete_menus', 'menus', '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(11, 'browse_roles', 'roles', '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(12, 'read_roles', 'roles', '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(13, 'edit_roles', 'roles', '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(14, 'add_roles', 'roles', '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(15, 'delete_roles', 'roles', '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(16, 'browse_users', 'users', '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(17, 'read_users', 'users', '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(18, 'edit_users', 'users', '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(19, 'add_users', 'users', '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(20, 'delete_users', 'users', '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(21, 'browse_settings', 'settings', '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(22, 'read_settings', 'settings', '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(23, 'edit_settings', 'settings', '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(24, 'add_settings', 'settings', '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(25, 'delete_settings', 'settings', '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(26, 'browse_hooks', NULL, '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(27, 'browse_answers', 'answers', '2019-08-01 04:14:08', '2019-08-01 04:14:08'),
(28, 'read_answers', 'answers', '2019-08-01 04:14:08', '2019-08-01 04:14:08'),
(29, 'edit_answers', 'answers', '2019-08-01 04:14:08', '2019-08-01 04:14:08'),
(30, 'add_answers', 'answers', '2019-08-01 04:14:08', '2019-08-01 04:14:08'),
(31, 'delete_answers', 'answers', '2019-08-01 04:14:08', '2019-08-01 04:14:08'),
(32, 'browse_areas', 'areas', '2019-08-01 04:14:16', '2019-08-01 04:14:16'),
(33, 'read_areas', 'areas', '2019-08-01 04:14:16', '2019-08-01 04:14:16'),
(34, 'edit_areas', 'areas', '2019-08-01 04:14:16', '2019-08-01 04:14:16'),
(35, 'add_areas', 'areas', '2019-08-01 04:14:16', '2019-08-01 04:14:16'),
(36, 'delete_areas', 'areas', '2019-08-01 04:14:16', '2019-08-01 04:14:16'),
(37, 'browse_courses', 'courses', '2019-08-01 04:14:22', '2019-08-01 04:14:22'),
(38, 'read_courses', 'courses', '2019-08-01 04:14:22', '2019-08-01 04:14:22'),
(39, 'edit_courses', 'courses', '2019-08-01 04:14:22', '2019-08-01 04:14:22'),
(40, 'add_courses', 'courses', '2019-08-01 04:14:22', '2019-08-01 04:14:22'),
(41, 'delete_courses', 'courses', '2019-08-01 04:14:22', '2019-08-01 04:14:22'),
(42, 'browse_courses_areas', 'courses_areas', '2019-08-01 04:15:51', '2019-08-01 04:15:51'),
(43, 'read_courses_areas', 'courses_areas', '2019-08-01 04:15:51', '2019-08-01 04:15:51'),
(44, 'edit_courses_areas', 'courses_areas', '2019-08-01 04:15:51', '2019-08-01 04:15:51'),
(45, 'add_courses_areas', 'courses_areas', '2019-08-01 04:15:51', '2019-08-01 04:15:51'),
(46, 'delete_courses_areas', 'courses_areas', '2019-08-01 04:15:51', '2019-08-01 04:15:51'),
(47, 'browse_questions', 'questions', '2019-08-01 04:16:02', '2019-08-01 04:16:02'),
(48, 'read_questions', 'questions', '2019-08-01 04:16:02', '2019-08-01 04:16:02'),
(49, 'edit_questions', 'questions', '2019-08-01 04:16:02', '2019-08-01 04:16:02'),
(50, 'add_questions', 'questions', '2019-08-01 04:16:02', '2019-08-01 04:16:02'),
(51, 'delete_questions', 'questions', '2019-08-01 04:16:02', '2019-08-01 04:16:02'),
(52, 'browse_questions_videos', 'questions_videos', '2019-08-01 04:16:20', '2019-08-01 04:16:20'),
(53, 'read_questions_videos', 'questions_videos', '2019-08-01 04:16:20', '2019-08-01 04:16:20'),
(54, 'edit_questions_videos', 'questions_videos', '2019-08-01 04:16:20', '2019-08-01 04:16:20'),
(55, 'add_questions_videos', 'questions_videos', '2019-08-01 04:16:20', '2019-08-01 04:16:20'),
(56, 'delete_questions_videos', 'questions_videos', '2019-08-01 04:16:20', '2019-08-01 04:16:20'),
(57, 'browse_resources', 'resources', '2019-08-01 04:16:29', '2019-08-01 04:16:29'),
(58, 'read_resources', 'resources', '2019-08-01 04:16:29', '2019-08-01 04:16:29'),
(59, 'edit_resources', 'resources', '2019-08-01 04:16:29', '2019-08-01 04:16:29'),
(60, 'add_resources', 'resources', '2019-08-01 04:16:29', '2019-08-01 04:16:29'),
(61, 'delete_resources', 'resources', '2019-08-01 04:16:29', '2019-08-01 04:16:29'),
(62, 'browse_simulator_histories', 'simulator_histories', '2019-08-01 04:16:37', '2019-08-01 04:16:37'),
(63, 'read_simulator_histories', 'simulator_histories', '2019-08-01 04:16:37', '2019-08-01 04:16:37'),
(64, 'edit_simulator_histories', 'simulator_histories', '2019-08-01 04:16:37', '2019-08-01 04:16:37'),
(65, 'add_simulator_histories', 'simulator_histories', '2019-08-01 04:16:37', '2019-08-01 04:16:37'),
(66, 'delete_simulator_histories', 'simulator_histories', '2019-08-01 04:16:37', '2019-08-01 04:16:37'),
(67, 'browse_simulator_histories_questions', 'simulator_histories_questions', '2019-08-01 04:16:58', '2019-08-01 04:16:58'),
(68, 'read_simulator_histories_questions', 'simulator_histories_questions', '2019-08-01 04:16:58', '2019-08-01 04:16:58'),
(69, 'edit_simulator_histories_questions', 'simulator_histories_questions', '2019-08-01 04:16:58', '2019-08-01 04:16:58'),
(70, 'add_simulator_histories_questions', 'simulator_histories_questions', '2019-08-01 04:16:58', '2019-08-01 04:16:58'),
(71, 'delete_simulator_histories_questions', 'simulator_histories_questions', '2019-08-01 04:16:58', '2019-08-01 04:16:58'),
(72, 'browse_users_areas', 'users_areas', '2019-08-01 04:17:32', '2019-08-01 04:17:32'),
(73, 'read_users_areas', 'users_areas', '2019-08-01 04:17:32', '2019-08-01 04:17:32'),
(74, 'edit_users_areas', 'users_areas', '2019-08-01 04:17:32', '2019-08-01 04:17:32'),
(75, 'add_users_areas', 'users_areas', '2019-08-01 04:17:32', '2019-08-01 04:17:32'),
(76, 'delete_users_areas', 'users_areas', '2019-08-01 04:17:32', '2019-08-01 04:17:32'),
(77, 'browse_videos', 'videos', '2019-08-01 04:17:41', '2019-08-01 04:17:41'),
(78, 'read_videos', 'videos', '2019-08-01 04:17:41', '2019-08-01 04:17:41'),
(79, 'edit_videos', 'videos', '2019-08-01 04:17:41', '2019-08-01 04:17:41'),
(80, 'add_videos', 'videos', '2019-08-01 04:17:41', '2019-08-01 04:17:41'),
(81, 'delete_videos', 'videos', '2019-08-01 04:17:41', '2019-08-01 04:17:41'),
(87, 'browse_permissions', 'permissions', '2019-09-03 06:00:47', '2019-09-03 06:00:47'),
(88, 'read_permissions', 'permissions', '2019-09-03 06:00:47', '2019-09-03 06:00:47'),
(89, 'edit_permissions', 'permissions', '2019-09-03 06:00:47', '2019-09-03 06:00:47'),
(90, 'add_permissions', 'permissions', '2019-09-03 06:00:47', '2019-09-03 06:00:47'),
(91, 'delete_permissions', 'permissions', '2019-09-03 06:00:47', '2019-09-03 06:00:47'),
(92, 'disable_users', 'users', '2019-09-03 06:08:50', '2019-09-03 06:40:02'),
(93, 'update_account_users', 'users', '2019-09-08 03:42:31', '2019-09-08 03:42:31'),
(94, 'enable_users', 'users', '2019-09-19 17:50:21', '2019-09-19 17:50:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(16, 2),
(17, 1),
(17, 2),
(18, 1),
(18, 2),
(19, 1),
(19, 2),
(20, 1),
(20, 2),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(67, 1),
(68, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 3),
(94, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `questions`
--

CREATE TABLE `questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answers_quantity` int(11) NOT NULL DEFAULT '0',
  `course_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `questions`
--

INSERT INTO `questions` (`id`, `question`, `answers_quantity`, `course_id`, `created_at`, `updated_at`) VALUES
(9, '¿Quien vino primero, el huevo o la gallina?', 2, 1, '2019-08-26 05:04:33', '2019-09-02 06:56:58'),
(10, '¿Cuanto es 2+2?', 4, 1, '2019-09-02 06:56:23', '2019-09-21 01:22:41'),
(11, 'La pregunta del millon', 1, 2, '2019-09-02 22:58:28', '2019-09-02 22:59:24'),
(12, '√ 25', 1, 1, '2019-09-21 01:27:38', '2019-09-21 01:27:53'),
(13, '(x^2)^3', 1, 1, '2019-09-21 01:28:18', '2019-09-21 01:30:43'),
(14, '(6+5)', 1, 1, '2019-09-21 01:32:35', '2019-09-21 01:32:44'),
(15, '(2x)+(3x)+x', 1, 1, '2019-09-21 01:33:07', '2019-09-21 01:33:19'),
(16, '6*3*8', 1, 1, '2019-09-21 01:33:50', '2019-09-21 01:34:02'),
(17, '3+3', 1, 1, '2019-09-21 01:55:51', '2019-09-21 01:56:03'),
(18, '2x+y=x', 2, 1, '2019-09-21 01:56:41', '2019-09-21 01:57:16'),
(19, '6÷2', 1, 1, '2019-09-21 01:57:54', '2019-09-21 01:58:03'),
(20, '8*4-2', 1, 1, '2019-09-21 01:58:30', '2019-09-21 01:58:43'),
(22, 'F1', 0, 3, '2019-09-21 02:02:44', '2019-09-21 02:02:44'),
(23, 'F2', 0, 3, '2019-09-21 02:02:51', '2019-09-21 02:02:51'),
(24, 'F3', 0, 3, '2019-09-21 02:02:59', '2019-09-21 02:02:59'),
(25, 'F4', 0, 3, '2019-09-21 02:03:08', '2019-09-21 02:03:08'),
(26, 'F5', 0, 3, '2019-09-21 02:03:17', '2019-09-21 02:03:17'),
(27, 'F6', 0, 3, '2019-09-21 02:03:24', '2019-09-21 02:03:24'),
(28, 'F7', 0, 3, '2019-09-21 02:04:03', '2019-09-21 02:04:03'),
(29, 'F8', 0, 3, '2019-09-21 02:04:11', '2019-09-21 02:04:11'),
(30, 'F9', 0, 3, '2019-09-21 02:04:19', '2019-09-21 02:04:19'),
(31, 'F10', 0, 3, '2019-09-21 02:04:26', '2019-09-21 02:04:26'),
(32, 'F11', 0, 3, '2019-09-21 02:04:32', '2019-09-21 02:04:32'),
(33, 'Q1', 0, 2, '2019-09-21 02:13:36', '2019-09-21 02:13:36'),
(34, 'Q2', 0, 2, '2019-09-21 02:13:44', '2019-09-21 02:13:44'),
(35, 'Q3', 0, 2, '2019-09-21 02:13:52', '2019-09-21 02:13:52'),
(36, 'Q4', 0, 2, '2019-09-21 02:14:14', '2019-09-21 02:14:14'),
(37, 'Q5', 0, 2, '2019-09-21 02:14:20', '2019-09-21 02:14:20'),
(38, 'Q6', 0, 2, '2019-09-21 02:14:26', '2019-09-21 02:14:26'),
(39, 'Q7', 0, 2, '2019-09-21 02:14:33', '2019-09-21 02:14:33'),
(40, 'Q8', 0, 2, '2019-09-21 02:14:40', '2019-09-21 02:14:40'),
(41, 'Q9', 0, 2, '2019-09-21 02:14:46', '2019-09-21 02:14:46'),
(42, 'Q10', 0, 2, '2019-09-21 02:14:53', '2019-09-21 02:14:53'),
(43, 'Q11', 0, 2, '2019-09-21 02:15:00', '2019-09-21 02:15:00'),
(44, 'I1', 0, 9, '2019-09-21 02:15:54', '2019-09-21 02:15:54'),
(45, 'I2', 0, 9, '2019-09-21 02:16:00', '2019-09-21 02:16:00'),
(46, 'I3', 0, 9, '2019-09-21 02:16:07', '2019-09-21 02:16:07'),
(47, 'I4', 0, 9, '2019-09-21 02:16:15', '2019-09-21 02:16:15'),
(48, 'I5', 0, 9, '2019-09-21 02:16:23', '2019-09-21 02:16:23'),
(49, 'I6', 0, 9, '2019-09-21 02:16:31', '2019-09-21 02:16:31'),
(50, 'I7', 0, 9, '2019-09-21 02:16:37', '2019-09-21 02:16:37'),
(51, 'I8', 0, 9, '2019-09-21 02:16:43', '2019-09-21 02:16:43'),
(52, 'I9', 0, 9, '2019-09-21 02:16:50', '2019-09-21 02:16:50'),
(53, 'I10', 0, 9, '2019-09-21 02:16:59', '2019-09-21 02:16:59'),
(54, 'I11', 0, 9, '2019-09-21 02:17:06', '2019-09-21 02:17:06'),
(55, 'I12', 0, 9, '2019-09-21 02:17:15', '2019-09-21 02:17:15'),
(56, 'C1', 0, 10, '2019-09-21 02:17:46', '2019-09-21 02:17:46'),
(57, 'C2', 0, 10, '2019-09-21 02:17:53', '2019-09-21 02:17:53'),
(58, 'C3', 0, 10, '2019-09-21 02:18:00', '2019-09-21 02:18:00'),
(59, 'C4', 0, 10, '2019-09-21 02:18:06', '2019-09-21 02:18:06'),
(60, 'C5', 0, 10, '2019-09-21 02:18:12', '2019-09-21 02:18:12'),
(61, 'C6', 0, 10, '2019-09-21 02:18:18', '2019-09-21 02:18:18'),
(62, 'C7', 0, 10, '2019-09-21 02:18:32', '2019-09-21 02:18:32'),
(63, 'C8', 0, 10, '2019-09-21 02:18:39', '2019-09-21 02:18:39'),
(64, 'C9', 0, 10, '2019-09-21 02:18:46', '2019-09-21 02:18:46'),
(65, 'C10', 0, 10, '2019-09-21 02:18:57', '2019-09-21 02:18:57'),
(66, 'C11', 0, 10, '2019-09-21 02:19:10', '2019-09-21 02:19:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `questions_videos`
--

CREATE TABLE `questions_videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `question_id` int(10) UNSIGNED NOT NULL,
  `video_id` int(10) UNSIGNED NOT NULL,
  `answer_link_description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `questions_videos`
--

INSERT INTO `questions_videos` (`id`, `question_id`, `video_id`, `answer_link_description`) VALUES
(1, 9, 1, 'Esta es la descripcion del video'),
(2, 11, 1, 'Esta es la descripcion del video');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resources`
--

CREATE TABLE `resources` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `course_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `resources`
--

INSERT INTO `resources` (`id`, `name`, `description`, `uri`, `created_at`, `updated_at`, `course_id`) VALUES
(1, 'Introduccion a flutter', 'Flutter es el nuevo Framework para desarrollo de aplicaciones hibridas, a través de este documento podrás ver contenido introducctorio al curso plasmado.', '[{\"download_link\":\"resources\\\\August2019\\\\jAPaL85d2xr1GepjV1pE.pdf\",\"original_name\":\"flutter.pdf\"}]', '2019-08-25 05:27:00', '2019-09-02 07:54:26', 10),
(7, 'Banner', 'Este es la descripcion del banner', '[{\"download_link\":\"resources\\\\September2019\\\\344NgnyKbs8bBmIWFAQY.jpg\",\"original_name\":\"bg_project_artur.jpg\"}]', '2019-09-02 07:45:26', '2019-09-02 07:45:26', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2019-08-01 04:07:20', '2019-08-01 04:07:20'),
(2, 'assistant', 'Asistente', '2019-08-01 04:07:20', '2019-08-26 05:07:49'),
(3, 'student', 'Estudiante', '2019-08-04 03:51:18', '2019-08-04 03:51:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin'),
(12, 'student.inicio', 'Student initial screen title', NULL, NULL, 'text', 6, 'Student'),
(15, 'student.Mis Cursos', 'Student courses screen title', '', NULL, 'text', 7, 'Student'),
(16, 'student.Mi perfil', 'Student profile screen title', '', NULL, 'text', 8, 'Student');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `simulator_histories`
--

CREATE TABLE `simulator_histories` (
  `id` int(10) UNSIGNED NOT NULL,
  `score` int(11) NOT NULL,
  `spent_time` time NOT NULL,
  `status` tinyint(4) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `simulator_histories_questions`
--

CREATE TABLE `simulator_histories_questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `question_id` int(10) UNSIGNED NOT NULL,
  `answer_id` int(10) UNSIGNED NOT NULL,
  `simulator_history_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direction` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default-student.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ci` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `phone`, `direction`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`, `ci`, `active`) VALUES
(1, 1, 'Administrador', 'admin@admin.com', NULL, NULL, 'users/default-student.png', NULL, '$2y$10$4YdJOcNZZhLbKX4iKx5PT.a0EFXI2P0aIx0XuPTlVGEVI4PILC.RW', 'lUqS54VeU9UT0W7LjfK6nGc1gVZJ1sQ48iw9MHR6VTnlxBAa4ZvQ7KjFWtCJ', NULL, '2019-08-03 20:27:39', '2019-09-14 20:52:55', NULL, 1),
(2, 3, 'Luis', 'luis@gmail.com', NULL, NULL, 'users/default-student.png', NULL, '$2y$10$VklAFepyi9aRD/wOPAtF6.BALyli.ufy6AY478/awiA1jZMG/W4Ci', 'h1BSoyV7PqH5FZENST61z1CB8dK0CmVDKW5LFyMYFXSLovnTBatypwMs5HuR', '{\"locale\":\"en\"}', '2019-08-04 03:55:52', '2019-09-15 00:21:10', NULL, 0),
(3, 3, 'Carlos', 'carlos@gmail.com', NULL, NULL, 'users/default-student.png', NULL, '$2y$10$4x8cY1DkfRvRkmgQ1zncSe7LJ66g./NHPfVCbLsmte65gEdjqF49W', NULL, '{\"locale\":\"en\"}', '2019-08-04 03:56:16', '2019-09-15 00:21:10', NULL, 0),
(4, 3, 'Jose Rojas', 'jose@gmail.com', NULL, NULL, 'users/default-student.png', NULL, '$2y$10$TMCBR8AWP076ObvFTLqeIOuBn6pqY4Q2Lp4QvwtZeKUgroY3XRska', NULL, '{\"locale\":\"en\"}', '2019-08-04 03:56:40', '2019-09-15 00:21:10', NULL, 0),
(5, 3, 'Paul Fernando Grimaldo', 'paulgrimaldo@hotmail.com', '69000850', 'Av Alemana', 'users/DP1l752fV5hBYlFl2x3xCvQKsqV6WgA9GgdEqKT9.png', NULL, '$2y$10$tI6z2ssS6hKmYcBhFTueCuJtnCYYBZo6bn2WAijLtQ8UW3/5FOGHe', 'N2ubc8BppT9QjBHgEC9JgQCqu4HAccjOTatAjXJVGxfeb1XdURU9SKHEBsjt', '{\"locale\":\"en\"}', '2019-08-09 07:26:08', '2019-09-15 00:21:10', '9796898', 0),
(7, 3, 'Carlos', 'carlos@carlos.com', NULL, NULL, 'users/default-student.png', NULL, '$2y$10$GLunt1HU8F6wUwOUmtiSre/WaeSvqiCgIbGDZYtargJVBcK2YnGpO', 'uXWcD3GWR8eCHTz7Fnazfqifh9hdc7Zjbu3N2sEYzECfyFIuznsVttXbHqr0', NULL, '2019-08-25 05:05:10', '2019-09-15 00:21:10', NULL, 0),
(8, 2, 'Asistente', 'asistente@asistente.com', NULL, NULL, 'users/default-student.png', NULL, '$2y$10$.IdPxQY7HcfFiptKtxHicuy4gJm7YvGvXZcqAQXXYmwAqnBgB9R2C', NULL, NULL, '2019-08-26 05:08:58', '2019-09-02 08:11:22', NULL, 1),
(9, 2, 'adasdas', 'dadad@dadad.com', NULL, NULL, 'users\\September2019\\YZuG0Gx1WMI3pCwgFyCz.png', NULL, '$2y$10$m7tIZDgweHNQJgPRseVyce8/pjjJG6NG2ofXpULGt/1U636J0G2vq', NULL, NULL, '2019-09-08 02:07:44', '2019-09-14 23:32:16', NULL, 1),
(10, 3, 'Fabio Cortez', 'fabiocm_08@hotmail.com', '75615845', 'Av Internacional', 'users\\September2019\\1A8EO579VVSpgqWSQ3tz.png', NULL, '$2y$10$YOM5GAUizYARKuToUghBRODycG0Kx9tilU1gwIuFRXMTgC7zXnG.a', NULL, NULL, '2019-09-14 20:53:39', '2019-09-15 00:21:10', '8913657', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_areas`
--

CREATE TABLE `users_areas` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `area_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users_areas`
--

INSERT INTO `users_areas` (`id`, `user_id`, `area_id`) VALUES
(3, 5, 3),
(4, 2, 3),
(5, 3, 3),
(6, 5, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

CREATE TABLE `videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `videos`
--

INSERT INTO `videos` (`id`, `name`, `description`, `uri`, `course_id`, `created_at`, `updated_at`) VALUES
(1, 'Clase # 1 de Psicologia', 'La adolescencia a', '[{\"download_link\":\"videos\\\\September2019\\\\Qwpc89brj900kAfWIPHd.mp4\",\"original_name\":\"psicologia 7 opening_x264.mp4\"}]', 6, '2019-09-01 04:26:56', '2019-09-01 04:31:30');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `answers_question_id_foreign` (`question_id`);

--
-- Indices de la tabla `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `courses_areas`
--
ALTER TABLE `courses_areas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `courses_areas_course_id_foreign` (`course_id`),
  ADD KEY `courses_areas_area_id_foreign` (`area_id`);

--
-- Indices de la tabla `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indices de la tabla `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indices de la tabla `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indices de la tabla `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indices de la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indices de la tabla `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questions_course_id_foreign` (`course_id`);

--
-- Indices de la tabla `questions_videos`
--
ALTER TABLE `questions_videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questions_videos_question_id_foreign` (`question_id`),
  ADD KEY `questions_videos_video_id_foreign` (`video_id`);

--
-- Indices de la tabla `resources`
--
ALTER TABLE `resources`
  ADD PRIMARY KEY (`id`),
  ADD KEY `resources_course_id_index` (`course_id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indices de la tabla `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indices de la tabla `simulator_histories`
--
ALTER TABLE `simulator_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `simulator_histories_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `simulator_histories_questions`
--
ALTER TABLE `simulator_histories_questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `simulator_histories_questions_question_id_foreign` (`question_id`),
  ADD KEY `simulator_histories_questions_answer_id_foreign` (`answer_id`),
  ADD KEY `simulator_histories_questions_simulator_history_id_foreign` (`simulator_history_id`);

--
-- Indices de la tabla `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `users_areas`
--
ALTER TABLE `users_areas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_areas_user_id_foreign` (`user_id`),
  ADD KEY `users_areas_area_id_foreign` (`area_id`);

--
-- Indices de la tabla `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- Indices de la tabla `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `videos_course_id_foreign` (`course_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `areas`
--
ALTER TABLE `areas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `courses_areas`
--
ALTER TABLE `courses_areas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT de la tabla `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT de la tabla `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT de la tabla `questions_videos`
--
ALTER TABLE `questions_videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `resources`
--
ALTER TABLE `resources`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `simulator_histories`
--
ALTER TABLE `simulator_histories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `simulator_histories_questions`
--
ALTER TABLE `simulator_histories_questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `users_areas`
--
ALTER TABLE `users_areas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `answers_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `courses_areas`
--
ALTER TABLE `courses_areas`
  ADD CONSTRAINT `courses_areas_area_id_foreign` FOREIGN KEY (`area_id`) REFERENCES `areas` (`id`),
  ADD CONSTRAINT `courses_areas_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`);

--
-- Filtros para la tabla `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`);

--
-- Filtros para la tabla `questions_videos`
--
ALTER TABLE `questions_videos`
  ADD CONSTRAINT `questions_videos_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`),
  ADD CONSTRAINT `questions_videos_video_id_foreign` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`);

--
-- Filtros para la tabla `simulator_histories`
--
ALTER TABLE `simulator_histories`
  ADD CONSTRAINT `simulator_histories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `simulator_histories_questions`
--
ALTER TABLE `simulator_histories_questions`
  ADD CONSTRAINT `simulator_histories_questions_answer_id_foreign` FOREIGN KEY (`answer_id`) REFERENCES `answers` (`id`),
  ADD CONSTRAINT `simulator_histories_questions_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`),
  ADD CONSTRAINT `simulator_histories_questions_simulator_history_id_foreign` FOREIGN KEY (`simulator_history_id`) REFERENCES `simulator_histories` (`id`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Filtros para la tabla `users_areas`
--
ALTER TABLE `users_areas`
  ADD CONSTRAINT `users_areas_area_id_foreign` FOREIGN KEY (`area_id`) REFERENCES `areas` (`id`),
  ADD CONSTRAINT `users_areas_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `videos`
--
ALTER TABLE `videos`
  ADD CONSTRAINT `videos_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
