<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimulatorHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('simulator_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('score');
            $table->time('spent_time');
            $table->TinyInteger('status');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('area_id');

            $table->foreign('area_id')
            ->references('id')->on('areas')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('simulator_histories');
    }
}
