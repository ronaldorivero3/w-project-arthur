<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 14-Sep-19
 * Time: 5:14 PM
 */

namespace App;

class FileStreamer
{

    public static function streamFile($contentType, $path)
    {
        $fullsize = filesize($path);
        $size = $fullsize;
        $stream = fopen($path, "r");
        $response_code = 200;
        $headers = array("Content-type" => $contentType);

        // Check for request for part of the stream
        $range = \Request::header('Range');
        if ($range != null) {
            $eqPos = strpos($range, "=");
            $toPos = strpos($range, "-");
            $unit = substr($range, 0, $eqPos);
            $start = intval(substr($range, $eqPos + 1, $toPos));
            $success = fseek($stream, $start);
            if ($success == 0) {
                $size = $fullsize - $start;
                $response_code = 206;
                $headers["Accept-Ranges"] = $unit;
                $headers["Content-Range"] = $unit . " " . $start . "-" . ($fullsize - 1) . "/" . $fullsize;
            }
        }

        $headers["Content-Length"] = $size;

        return \Response::stream(function () use ($stream) {
            fpassthru($stream);
        }, $response_code, $headers);
    }
}
