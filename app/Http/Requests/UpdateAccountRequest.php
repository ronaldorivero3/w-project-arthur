<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermission('update_account_users');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = auth()->id();
        return [
            'name' => 'required|max:255',
            'email' => 'required|string|email|max:191|unique:users,email,' . $id,
            'phone' => 'nullable|numeric',
            'direction' => 'nullable|max:255',
            'avatar' => 'nullable|image|max:3000',
            'ci' => 'required|max:255|unique:users,ci,' . $id,
        ];
    }

    public function messages()
    {
        return [
            'name.required' => __('The field name is obligatory'),
            'email.required' => __('The field email is obligatory'),
            'email.unique' => __('This email is not available'),
            'phone.numeric' => __('Only numbers are accepted in this field'),
            'avatar.image' => __('The field avatar only accept images'),
            'avatar.max' => __('Image too weight, please select another'),
            'ci.required' => __('The field CI is obligatory'),
            'ci.unique' => __('This CI is already registered'),
            'max' => __('Max is 255 characters, please summarize your content')
        ];
    }
}
