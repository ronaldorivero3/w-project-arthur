<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class SiteController extends Controller
{
    public function getPrivacyPolicies()
    {
        return view('site.privacy_policies');
    }

    public function getConditionsTerms()
    {
        return view('site.conditions_terms');
    }
}
