<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class UserController extends VoyagerBaseController
{

    public function disable()
    {
        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', 'users')->first();
        // Check permission
        $this->authorize('disable', app($dataType->model_name));
        User::whereIn('role_id', function ($query) {
            $query->select('roles.id')->from('roles')
                ->where('roles.name', 'like', "%student%");
        })->update(['active' => 0]);
        return redirect()->route("voyager.{$dataType->slug}.index")->with([
            'message' => __('Users successfully disabled'),
            'alert-type' => 'success',
        ]);
    }

    public function enable()
    {
        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', 'users')->first();
        // Check permission
        $this->authorize('enable', app($dataType->model_name));
        User::whereIn('role_id', function ($query) {
            $query->select('roles.id')->from('roles')
                ->where('roles.name', 'like', "%student%");
        })->update(['active' => 1]);
        return redirect()->route("voyager.{$dataType->slug}.index")->with([
            'message' => __('Users successfully enable'),
            'alert-type' => 'success',
        ]);
    }
}
