<?php

namespace App\Http\Controllers\Admin;

use App\Models\Answer;
use App\Models\Question;
use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class AnswerController extends VoyagerBaseController
{
    public function store(Request $request)
    {
        $redirectResponse = parent::store($request);
        try {
            $question = Question::findOrFail($request->question_id);
            $question->addAnswersQuantity();
        } catch (\Exception $ignored) {
        }
        return $redirectResponse;
    }

    public function destroy(Request $request, $id)
    {
        try {
            $answer = Answer::findOrFail($id);
            $answer->question->reduceAnswersQuantity();
        } catch (\Exception $ignored) {
        }
        return parent::destroy($request, $id);
    }

    /***
     * Reduce one answer from current question and add one answer to new selected question then update
     * with the request
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $answer = Answer::findOrFail($id);
            $questionId = $request->question_id;
            if ($questionId != $answer->question_id) {
                $answer->question->reduceAnswersQuantity();
                Question::findOrFail($questionId)->addAnswersQuantity();
            }
        } catch (\Exception $ignored) {
        }
        return parent::update($request, $id);
    }
}
