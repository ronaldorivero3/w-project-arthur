<?php

namespace App\Http\Controllers\Users;

use App\FileStreamer;
use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\Video;

class VideoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getVideo(Video $video)
    {
        $path_to_file = join(DIRECTORY_SEPARATOR, array(storage_path(), 'app', 'public', $video->parsed_uri));
        return FileStreamer::streamFile("video/mp4", $path_to_file);
    }

    public function showVideo(Area $area, Video $video)
    {
        $course = $video->course;
        return view('users.courses.show.video_player', compact('video', 'course', 'area'));
    }
}
