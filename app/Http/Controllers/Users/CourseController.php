<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\Course;
use App\Models\Courses\CourseContentResolver;
use App\Models\Courses\ContentResolverFactory;
use App\Models\Resource;
use App\Models\Video;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function show(Area $area, Course $course, $contenido = 'videos')
    {
        $strategy = ContentResolverFactory::getContentResolver($contenido);
        $contentResolver = new CourseContentResolver($strategy);
        return $contentResolver->resolveContent($area, $course);
    }

    public function getVideos(Request $request, Course $course)
    {
        if (!$request->ajax()) {
            abort(403);
        }
        return response()->json($course->videos);
    }

    public function getResources(Request $request, Course $course)
    {
        if (!$request->ajax()) {
            abort(403);
        }
        return response()->json($course->resources);
    }
}
