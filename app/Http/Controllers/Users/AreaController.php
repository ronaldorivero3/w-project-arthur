<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Area;

class AreaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(Area $area)
    {
        $courses = $area->courses;
        return view('users.courses.index', compact('courses', 'area'));
    }
}
