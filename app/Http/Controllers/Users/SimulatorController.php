<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\Course as AppCourse;
use App\Models\QuestionVideo;
use App\Models\Simulator\Answer;
use App\Models\Simulator\Course;
use App\Models\Simulator\Question;
use App\Models\Simulator\SimulatedArea;
use App\Models\Simulator\Simulator;
use App\Models\SimulatorHistory;
use App\Models\SimulatorHistoryQuestion;
use Exception;
use Illuminate\Http\Request;

class SimulatorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $simulatorHistories = auth()
            ->user()
            ->simulator_histories()
            ->orderBy('created_at', 'desc')
            ->paginate(10);
        return view('users.simulator.histories', compact('simulatorHistories'));
    }

    public function showDetail(SimulatorHistory $simulatorHistory)
    {
        $area = $simulatorHistory->area;
        $courses = $area->courses;
        $simulator = new Simulator();
        $simulator->spentTime = $simulatorHistory->spent_time;
        $simulator->score = $simulatorHistory->score;
        $simulator->area = new SimulatedArea($area->id, $area->name);
        $simulator->finished = true;
        foreach ($courses as $course) {
            $simulatedCourse = new Course($course->id, $course->name);
            $simulatorHistoriesQuestions = $this->getSimulatorHistoriesQuestion($course, $simulatorHistory);
            foreach ($simulatorHistoriesQuestions as $simulatorHistoryQuestion) {
                $question = $simulatorHistoryQuestion->question;
                $answer = $simulatorHistoryQuestion->answer;
                $simulatedQuestion = new Question($question->id, $question->question);
                $simulatedQuestion->disableAnswerInputs();
                if ($answer != null) {
                    $simulatedQuestion->selectedAnswer = new Answer($answer->id, $answer->answer, $answer->isCorrect());
                    if ($answer->isCorrect()) {
                        $simulatedQuestion->enableSuccessMessage();
                    } else {
                        $suggestion = $question->question_video;
                        if ($suggestion) {
                            $simulatedQuestion->bindSuggestion($suggestion);
                        }
                    }
                } else {
                    $suggestion = $question->question_video;
                    if ($suggestion) {
                        $simulatedQuestion->bindSuggestion($suggestion);
                    }
                }
                foreach ($question->answer as $answer) {
                    $simulatedAnswer = new Answer($answer->id, $answer->answer, $answer->isCorrect());
                    $simulatedQuestion->addAnswer($simulatedAnswer);
                }
                $simulatedCourse->addQuestion($simulatedQuestion);
            }
            $simulator->addCourse($simulatedCourse);
        }
        return view('users.simulator.history_detail', compact('simulator'));
    }

    public function getSimulatorHistoriesQuestion(AppCourse $course, SimulatorHistory $simulatorHistory)
    {
        return SimulatorHistoryQuestion::select('simulator_histories_questions.*')
            ->join('questions', 'questions.id', 'simulator_histories_questions.question_id')
            ->join('courses', 'courses.id', 'questions.course_id')
            ->where('courses.id', '=', $course->id)
            ->where('simulator_histories_questions.simulator_history_id', '=', $simulatorHistory->id)
            ->get();
    }

    public function show(Area $area)
    {
        $courses = $area->courses;
        $simulator = new Simulator();
        $simulator->area = new SimulatedArea($area->id, $area->name);
        foreach ($courses as $course) {
            $simulatorCourse = new Course($course->id, $course->name);
            try {
                $questions = $course->questions->random(10);
            } catch (Exception $e) {
                return back()->with(['simulator_invalid_questions_number' => true]);
            }
            foreach ($questions as $question) {
                $simulatorQuestion = new Question($question->id, $question->question);
                foreach ($question->answer as $answer) {
                    $simulatorAnswer = new Answer($answer->id, $answer->answer, $answer->isCorrect());
                    $simulatorQuestion->addAnswer($simulatorAnswer);
                }
                $simulatorCourse->addQuestion($simulatorQuestion);
            }
            $simulator->addCourse($simulatorCourse);
        }
        session(['simulator' => $simulator]);
        return view('users.simulator.index', compact('simulator'));
    }

    public function cancel(Request $request)
    {
        if (!$request->ajax()) {
            return back();
        }
        session()->forget('simulator');
        return response()->json(['message' => 'Simulator cancelled successfully']);
    }

    public function store(Request $request)
    {
        if (!$request->ajax()) {
            return back();
        }
        $simulatorHistory = $this->saveNewSimulatorHistory($request);

        $simulator = $request->simulator;

        $totalCorrect = 0;

        $simulatorResult = new Simulator();
        foreach ($simulator['courses'] as $course) {
            $simulatedCourse = new Course($course['id'], $course['name']);

            foreach ($course['questions'] as $question) {
                $simulatedQuestion = new Question($question['id'], $question['question']);

                $simulatedQuestion->selectedAnswer = $question['selectedAnswer'];

                $simulatedQuestion->disableAnswerInputs();
                if ($question['selectedAnswer']['correct']) {
                    $totalCorrect++;

                    $simulatedQuestion->enableSuccessMessage();
                } else {
                    $suggestion = QuestionVideo::where('question_id', '=', $question['id'])->first();

                    if ($suggestion) {
                        $simulatedQuestion->bindSuggestion($suggestion);
                    }
                }

                $simulatedQuestion->answers = $question['answers'];
                $simulatedCourse->addQuestion($simulatedQuestion);
                $this->saveNewSimulatorHistoryQuestion($question, $simulatorHistory->id);
            }
            $simulatorResult->addCourse($simulatedCourse);
        }
        $this->updateSimulatorHistoryScore($simulatorHistory, $totalCorrect);
        $simulatorResult->area = $simulator['area'];
        $simulatorResult->spentTime = $simulator['spentTime'];
        $simulatorResult->score = $totalCorrect;
        $simulatorResult->finished = true;
        return response()->json(['simulator' => $simulatorResult]);
    }

    private function updateSimulatorHistoryScore(SimulatorHistory $simulatorHistory, $score)
    {
        $simulatorHistory->score = $score;
        $simulatorHistory->save();
    }

    private function saveNewSimulatorHistory(Request $request)
    {
        $simulatorHistory = new SimulatorHistory();
        $simulatorHistory->score = $request->simulator['score'];
        $simulatorHistory->spent_time = $request->simulator['spentTime'];
        $simulatorHistory->area_id = $request->simulator['area']['id'];
        $simulatorHistory->user_id = auth()->id();
        $simulatorHistory->status = 1;
        $simulatorHistory->save();
        return $simulatorHistory;
    }

    private function saveNewSimulatorHistoryQuestion($question, $simulatorId)
    {
        $simulatorHistoryQuestion = new SimulatorHistoryQuestion();
        $simulatorHistoryQuestion->simulator_history_id = $simulatorId;
        $simulatorHistoryQuestion->question_id = $question['id'];
        $simulatorHistoryQuestion->answer_id = $question['selectedAnswer']['id'];
        $simulatorHistoryQuestion->save();
        return $simulatorHistoryQuestion;
    }
}
