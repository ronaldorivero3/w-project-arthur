<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Area;
use App\Models\CourseArea;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{

    public function index()
    {
        if (auth()->check()) {
            $areas = auth()->user()->areas;
            return view('users.dashboard', compact('areas'));
        }

        $show_areas = DB::table('courses_areas')
            ->select('area_id', DB::raw('count(*) as total'))
            ->groupBy('area_id')
            ->orderByDesc('total')
            ->get();
        $mayor = $show_areas->first()->total;
        $aux = $show_areas->where('total', $mayor)->random(2);
        $number_areas = count($show_areas);
        $collection = [];
        $areas = new Collection();
        foreach ($aux as $area) {
            $total = $area->total;
            if ($total >= $mayor) {
                array_push($collection, $area->area_id);
                if (count($collection) > 1) {
                    break;
                }
            }

        }
        foreach ($collection as $aux) {
            $new_area = Area::all()->where('id', '=', $aux);
            $areas->push($new_area->first());
        }
        return view('welcome', compact('areas', 'number_areas'));
    }
}
