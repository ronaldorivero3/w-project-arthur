<?php

namespace App\Http\Controllers\Users;

use App\FileStreamer;
use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\Resource;

class ResourceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getResource(Resource $resource)
    {
        $path_to_file = join(DIRECTORY_SEPARATOR, array(storage_path(), 'app', 'public', $resource->parsed_uri));
        return FileStreamer::streamFile("application/pdf", $path_to_file);
    }

    public function showResource(Area $area, Resource $resource)
    {
        $course = $resource->course;
        return view('users.courses.show.resource_player', compact('resource', 'course', 'area'));
    }
}
