<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateAccountRequest;
use App\Models\Area;
use Illuminate\Support\Facades\Storage;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function profile()
    {
        $user = auth()->user();
        return view('users.account.profile', compact('user'));
    }

    public function update(UpdateAccountRequest $request)
    {
        $user = auth()->user();
        $user->name = $request->post('name');
        $user->email = $request->post('email');

        if ($request->has('phone')) {
            $user->phone = $request->post('phone');
        }

        if ($request->has('direction')) {
            $user->direction = $request->post('direction');
        }

        if ($request->has('ci')) {
            $user->ci = $request->post('ci');
        }

        if ($request->has('password')) {
            $user->password = bcrypt($request->post('password'));
        }

        if ($request->hasFile('avatar')) {
            if (!$user->isUsingDefaultAvatar()) {
                Storage::disk('public')->delete($user->avatar);
            }
            $user->avatar = Storage::disk('public')->put('users', $request->file('avatar'));
        }
        $user->save();
        return back()
            ->with([
                'message' => __('Account updated successfully'),
                'alert-type' => 'success',
            ]);
    }
}
