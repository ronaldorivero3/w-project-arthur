<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use PDOException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $isStudent = auth()->check() && auth()->user()->roles->contains('student');
        $viewComplement = ($isStudent)?'_student':'_admin';
        // 404 page when a model is not found
        if ($exception instanceof \Illuminate\Database\Eloquent\ModelNotFoundException) {
            return response()->view('errors.404'.$viewComplement, [], 404);
        }
        // custom error message
        if ($exception instanceof \Symfony\Component\Debug\Exception\FatalErrorException || $exception instanceof PDOException) {
            return response()->view('errors.500'.$viewComplement, [], 500);
        }

        if ($exception instanceof \Illuminate\Session\TokenMismatchException) {
            return redirect('/login');
        }
        return parent::render($request, $exception);
    }
}
