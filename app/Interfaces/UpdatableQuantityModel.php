<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 9/1/2019
 * Time: 10:31 PM
 */

namespace App\Interfaces;

interface UpdatableQuantityModel
{
    public function addByOne();

    public function reduceByOne();
}
