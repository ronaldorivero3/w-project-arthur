<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionVideo extends Model
{
    protected $table = 'questions_videos';
    protected $fillable = ['question_id', 'video_id', 'answer_link_description'];
    public $timestamps = false;
}
