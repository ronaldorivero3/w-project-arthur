<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    protected $table = 'resources';
    protected $fillable = ['name', 'description', 'uri', 'course_id'];
    protected $appends = ['parsed_uri'];

    public function getParsedUriAttribute()
    {
        return (json_decode($this->uri))[0]->download_link;
    }


    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }
}
