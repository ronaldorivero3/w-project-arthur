<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SimulatorHistoryQuestion extends Model
{
    protected $table = 'simulator_histories_questions';
    protected $fillable = ['question_id', 'answer_id', 'simulator_history_id'];

    public function question()
    {
        return $this->belongsTo(Question::class, 'question_id');
    }

    public function answer()
    {
        return $this->belongsTo(Answer::class, 'answer_id');
    }
}
