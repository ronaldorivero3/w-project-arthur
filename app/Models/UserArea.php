<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserArea extends Model
{
    protected $table = 'users_areas';
    protected $fillable = ['user_id', 'area_id'];
    public $timestamps = false;
}
