<?php

namespace App\Models\Courses;

use App\Models\Area;
use App\Models\Course;

class CourseContentResolver
{
    private $contentResolver;


    public function __construct(ContentResolver $contentResolver)
    {
        $this->contentResolver = $contentResolver;
    }

    public function setContentResolverStrategy(ContentResolver $contentResolver)
    {
        $this->contentResolver = $contentResolver;
    }

    public function resolveContent(Area $area, Course $course)
    {
        return $this->contentResolver->resolveContent($area, $course);
    }
}
