<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 15-Sep-19
 * Time: 1:30 PM
 */

namespace App\Models\Courses;

use App\Models\Area;
use App\Models\Course;

class NullResolverStrategy implements ContentResolver
{

    public function resolveContent(Area $area, Course $course)
    {
        return back()->with(['error' => __('Invalid content type')]);
    }
}
