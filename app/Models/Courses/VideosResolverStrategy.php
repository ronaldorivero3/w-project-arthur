<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 15-Sep-19
 * Time: 1:13 PM
 */


namespace App\Models\Courses;

use App\Models\Area;
use App\Models\Course;

class VideosResolverStrategy implements ContentResolver
{

    public function resolveContent(Area $area, Course $course)
    {
        $videos = $course->videos;
        return view('users.courses.show.videos', compact('course', 'videos', 'area'));
    }
}
