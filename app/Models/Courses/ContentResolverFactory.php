<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 15-Sep-19
 * Time: 1:14 PM
 */

namespace App\Models\Courses;

class ContentResolverFactory
{

    public static function getContentResolver($contentType)
    {
        if ($contentType == 'videos') {
            return new VideosResolverStrategy();
        } elseif ($contentType == 'practicos') {
            return new ResourcesResolverStrategy();
        } else {
            return new NullResolverStrategy();
        }
    }
}
