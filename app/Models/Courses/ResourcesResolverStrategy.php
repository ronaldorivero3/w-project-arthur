<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 15-Sep-19
 * Time: 1:13 PM
 */

namespace App\Models\Courses;


use App\Models\Area;
use App\Models\Course;

class ResourcesResolverStrategy implements ContentResolver
{


    public function resolveContent(Area $area, Course $course)
    {
        $resources = $course->resources;
        return view('users.courses.show.resources', compact('course', 'resources', 'area'));
    }
}
