<?php

namespace App\Models\Courses;

use App\Models\Area;
use App\Models\Course;

interface ContentResolver
{
    public function resolveContent(Area $area, Course $course);
}
