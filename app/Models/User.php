<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use App\Notifications\CustomResetPasswordNotification;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'direction', 'avatar', 'phone', 'role_id', 'ci', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function areas()
    {
        return $this->belongsToMany(Area::class, 'users_areas');
    }

    public function isUsingDefaultAvatar()
    {
        return $this->avatar == "users/default-student.png";
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomResetPasswordNotification($token));
    }

    public function simulator_histories()
    {
        return $this->hasMany(SimulatorHistory::class, 'user_id');
    }

    public function isDisabled()
    {
        return $this->active == 0;
    }
}
