<?php

namespace App\Models\Simulator;

class SuggestionConfig{

    public $suggestion = null; 
    public $setSuccessMessage = false;  
    public $disableAnswerInputs = false;

    public function __construct()
    {
    }

    public function bindSuggestion($suggestion){
        $this->suggestion = $suggestion;
    }

    public function enableSuccessMessage(){
        $this->setSuccessMessage = true;
    }

    public function disableInputs(){
        $this->disableAnswerInputs = true;
    }
}