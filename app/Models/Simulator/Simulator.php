<?php

namespace App\Models\Simulator;

use Carbon\Carbon;

class Simulator
{
    public $spentTime = 0;
    public $score = 0;
    public $startTime;
    public $area;
    public $courses;
    public $finished = false;
    /**
     * Simulator constructor.
     */
    public function __construct()
    {
        $this->courses = array();
        $this->startTime = Carbon::now()->toDateTimeString();
    }

    public function addCourse(Course $course)
    {
        array_push($this->courses, $course);
    }
}
