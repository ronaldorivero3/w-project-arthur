<?php

namespace App\Models\Simulator;

class Answer
{
    public $id;
    public $name;
    public $correct = false;

    /**
     * Answer constructor.
     * @param $id
     * @param $name
     * @param bool $correct
     */
    public function __construct($id, $name, bool $correct)
    {
        $this->id = $id;
        $this->name = $name;
        $this->correct = $correct;
    }
}
