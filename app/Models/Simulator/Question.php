<?php

namespace App\Models\Simulator;

class Question
{
    public $id;
    public $question;
    public $answers;
    public $selectedAnswer = null;
    public $suggestionConfig = null;
    /**
     * Question constructor.
     * @param $id
     * @param $question
     */
    public function __construct($id, $question)
    {
        $this->id = $id;
        $this->question = $question;
        $this->answers = array();
        $this->suggestionConfig = new SuggestionConfig();
    }

    public function addAnswer(Answer $answer)
    {
        array_push($this->answers, $answer);
    }

    public function bindSuggestion($suggestion)
    {
        $this->suggestionConfig->bindSuggestion($suggestion);
    }

    public function enableSuccessMessage()
    {
        $this->suggestionConfig->enableSuccessMessage();
    }

    public function disableAnswerInputs()
    {
        $this->suggestionConfig->disableInputs();
    }
}
