<?php

namespace App\Models\Simulator;

class Course
{
    public $id;
    public $name;
    public $questions;

    /**
     * Course constructor.
     * @param $id
     * @param $name
     */
    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
        $this->questions = array();
    }

    public function addQuestion(Question $question)
    {
        array_push($this->questions, $question);
    }
}
