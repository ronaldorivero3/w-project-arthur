<?php

namespace App\Models\Simulator;

class SimulatedArea
{
    public $id;
    public $name;

    /**
     * Area constructor.
     * @param $id
     * @param $name
     */
    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }
}
