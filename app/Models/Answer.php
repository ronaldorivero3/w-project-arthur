<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = 'answers';
    protected $fillable = ['answer', 'question_id', 'is_correct'];

    public function question()
    {
        return $this->belongsTo(Question::class, 'question_id');
    }

    public function simulatorHistoryQuestions()
    {
        return $this->hasMany(SimulatorHistoryQuestion::class, 'answer_id');
    }

    public function isCorrect()
    {
        return $this->is_correct == 1;
    }
}
