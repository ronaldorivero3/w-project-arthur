<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = 'areas';
    protected $fillable = ['name', 'description', 'banner'];
    protected $appends = ['formatted_date'];

    public function getFormattedDateAttribute()
    {
        return $this->created_at->format('M d, y');
    }

    public function courses()
    {
        return $this->belongsToMany(Course::class, 'courses_areas');
    }
}
