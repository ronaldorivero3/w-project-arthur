<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'questions';
    protected $fillable = ['question', 'answers_quantity', 'course_id'];

    public function addAnswersQuantity()
    {
        $this->answers_quantity = $this->answers_quantity + 1;
        $this->save();
    }

    public function reduceAnswersQuantity()
    {
        $this->answers_quantity = $this->answers_quantity - 1;
        $this->save();
    }

    public function answer()
    {
        return $this->hasMany(Answer::class);
    }

    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }

    public function question_video()
    {
        return $this->hasOne(QuestionVideo::class, 'question_id');
    }
}
