<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SimulatorHistory extends Model
{
    protected $table = 'simulator_histories';
    protected $fillable = ['score', 'spent_time', 'status', 'user_id','area_id'];

    public function simulator_histories_questions()
    {
        return $this->hasMany(SimulatorHistoryQuestion::class, 'simulator_history_id');
    }

    public function area()
    {
        return $this->belongsTo(Area::class, 'area_id');
    }
}
