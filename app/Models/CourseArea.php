<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseArea extends Model
{
    protected $table = 'courses_areas';
    protected $fillable = ['course_id', 'area_id'];
    public $timestamps = false;
}
