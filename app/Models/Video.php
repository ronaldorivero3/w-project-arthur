<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $table = 'videos';
    protected $fillable = ['name', 'description', 'uri', 'course_id'];
    protected $appends = ['parsed_uri'];

    public function getParsedUriAttribute()
    {
        return (json_decode($this->uri))[0]->download_link;
    }

    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }
}
