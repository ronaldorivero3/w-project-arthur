<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';
    protected $fillable = ['name', 'description', 'banner'];
    protected $dates = ['created_at'];
    protected $appends = ['formatted_date'];

    public function getFormattedDateAttribute()
    {
        return $this->created_at->format('M d, y');
    }

    public function videos()
    {
        return $this->hasMany(Video::class);
    }

    public function resources()
    {
        return $this->hasMany(Resource::class);
    }

    public function questions()
    {
        return $this->hasMany(Question::class, 'course_id');
    }
}
