require('./bootstrap');

window.Vue = require('vue');
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue);

Vue.component('video-component', require('./components/VideoComponent.vue').default);
Vue.component('resource-component', require('./components/ResourceComponent.vue').default);

Vue.component('confirmation-modal', require('./components/ConfirmationModal.vue').default);
Vue.component('warning-block', require('./components/WarningBlock.vue').default);
Vue.component('loading-modal', require('./components/LoadingModal.vue').default);
Vue.component('alert-modal', require('./components/AlertModal.vue').default);
Vue.component('base-button', require('./components/BaseButton.vue').default);
Vue.component('backward-forward-button', require('./components/BackwardForwardButton.vue').default);
Vue.component('black-button', require('./components/BlackButton.vue').default);
Vue.component('green-button', require('./components/GreenButton.vue').default);

Vue.component('simulator', require('./components/simulator/Simulator.vue').default);
Vue.component('simulator-header', require('./components/simulator/SimulatorHeader.vue').default);
Vue.component('score-result', require('./components/simulator/ScoreResult.vue').default);
Vue.component('course-item', require('./components/simulator/CourseItem.vue').default);
Vue.component('question-item', require('./components/simulator/QuestionItem.vue').default);
Vue.component('question-header', require('./components/simulator/QuestionHeader.vue').default);
Vue.component('answers-list', require('./components/simulator/AnswersList.vue').default);
Vue.component('answer-item', require('./components/simulator/AnswerItem.vue').default);

const app = new Vue({
    el: '#app',
});
