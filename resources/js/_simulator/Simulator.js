const axios = require("axios").default;
import {errors} from "../_commons/errors";

export const store = {
    store: (simulator, onSuccess, onError = null) => {
        axios
            .post("/finish-simulator", {simulator: simulator})
            .then(response => {
                onSuccess(response.data);
            })
            .catch(error => {
                console.log(error);
                if (onError != null) {
                    onError();
                }
            });
    },
    cancel: (onSuccess, onCancel = null) => {
        axios
            .post("/cancel-simulator")
            .then(response => {
                onSuccess();
            })
            .catch(error => {
                console.log(error);
                if (onCancel != null) {
                    onCancel();
                }
            });
    }
};

export const helper = {
    getFormattedEndTime: time => {
        let hours;
        let minutes;
        let seconds;
        hours = Math.abs(1 - time.getHours());
        const tmpMinutes = time.getMinutes();
        if (hours === 1) { // Spent more than 1 hour
            if (tmpMinutes < 30) {
                minutes = Math.abs(30 - tmpMinutes); //e.g 01:10, 01:20, etc
            } else {
                hours = 0;
                minutes = Math.abs(59 - tmpMinutes + 30); //e.g 00:35, 00:45, etv
            }
        } else {
            minutes = Math.abs(30 - tmpMinutes);
        }
        if (minutes < 10) {
            minutes = `0${minutes}`;
        }
        hours = `0${hours}`;
        seconds = 59 - time.getSeconds();
        if (seconds < 10) {
            seconds = `0${seconds}`;
        }
        return `${hours}:${minutes}:${seconds}`;
    },
    buildErrorSavingSimulatorMessage() {
        return "Ocurrió un error inesperado. \n Por favor reporta el siguiente código de error: " +
            errors.ERROR_SAVING_SIMULATOR;
    }
};
