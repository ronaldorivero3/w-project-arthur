@extends('layouts.app')
@section('title')
    {{__('Courses')}}
@endsection
@section('section_title')
    {{__('My Courses')}}
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div data-wow-delay="300ms"
                     class="section-heading text-center mx-auto wow fadeInUp margin-b-30 animated">
                    <h3>
                        <a href="{{route('inicio')}}">
                            <img class="icon_link" src="{{asset('/svg/arrow-left.svg')}}" alt="left_arrow">
                        </a>
                        {{$area->name}} - {{$area->description}}
                    </h3>
                    <span>
                        @if($courses->isNotEmpty())
                            {{$courses->count()}}
                            @if($courses->count()>1)
                                {{__('Courses')}}
                            @else
                                {{__('Course')}}
                            @endif
                        @endif
                    </span>
                    @if($courses->isNotEmpty())
                        <a href="{{route('simulator.show',$area->id)}}" class="btn academy-btn btn-sm mt-15">
                            <span style="color:white; font-weight: bold">
                                    {{__('Simulate Test')}}
                            </span>
                        </a>
                    @endif
                </div>
            </div>
        </div>
        @include('users.courses.partials.courses_list')
    </div>
    @include('users.courses.partials.simulator_error')
@endsection
