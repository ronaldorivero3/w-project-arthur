@if(session('simulator_invalid_questions_number'))
    @include('users.courses.partials.simulator_error_modal')
    @push('scripts')
        <script type="text/javascript">
            $(document).ready(function () {
                $('#simulatorErrorModal').modal('show');
            });
        </script>
    @endpush
@endif
