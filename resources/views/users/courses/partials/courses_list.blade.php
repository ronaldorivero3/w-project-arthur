<div class="row">
    @forelse($courses as $course)
        <div class="col-12 col-lg-6">
            <div class="single-top-popular-course d-flex align-items-center flex-wrap mb-30 wow fadeInUp"
                 data-wow-delay="400ms"
                 style="visibility: visible; animation-delay: 400ms; animation-name: fadeInUp;">
                <div class="popular-course-content">
                    <h5>{{$course->name}}</h5>
                    <span>{{$course->formatted_date}}</span>
                    <p>{{$course->description}}
                    </p>
                    <a href="{{route('courses.show',['area'=>$area->id,'course'=>$course->id])}}"
                       class="btn academy-btn btn-sm mt-15">
                        <span style="color:white; font-weight:bold;">
                            {{__('See More')}}
                        </span>
                    </a>
                </div>
                <div class="popular-course-thumb bg-img"
                     style="background-image: url('{{ Voyager::image( $course->banner) }}');">
                </div>
            </div>
        </div>
    @empty
        @include('layouts.empty_content_indicator')
    @endforelse
</div>
