<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="simulatorErrorModal" id="simulatorErrorModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="loginModalLabel">{{ __('Simulator error') }}</h4>
                <img width="153" height="27" src="{{Voyager::image(setting('site.logo'))}}" alt="logo">
            </div>
            <div class="modal-body">
                <h5>{{__("The simulator can't start because there aren't questions available")}}</h5>
            </div>
            <div class="modal-footer">
                <button href="#" data-dismiss="modal" class="btn academy-btn btn-2" style="color:white;">
                    {{__('Accept')}}
                </button>
            </div>
        </div>
    </div>
</div>
