@extends('layouts.app')
@section('title')
    {{$course->name}}
@endsection
@section('section_title')
    {{__('Course')}} - {{$course->name}}
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div data-wow-delay="300ms"
                     class="section-heading text-center mx-auto wow fadeInUp margin-b-30 animated">
                    <h3>
                        <a href="{{route('areas.show',$area->id)}}">
                            <img class="icon_link" src="{{asset('/svg/arrow-left.svg')}}" alt="left_arrow">
                        </a>
                        {{__('Videos of ').$course->name}}
                    </h3>
                    <a href="{{route('courses.show',['area'=>$area->id,'course'=>$course->id,'contenido'=>'practicos'])}}"
                       role="button" class="btn academy-btn btn-4 m-2 a-button">
                        {{__('Go to practices')}}
                    </a>
                </div>
            </div>
        </div>
        @include('users.courses.show.videos_list')
    </div>
@endsection
