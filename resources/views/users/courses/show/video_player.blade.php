@extends('layouts.app')
@section('title')
    {{$video->name}}
@endsection
@section('section_title')
    {{__('Video')}} - {{$video->name}}
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div data-wow-delay="300ms"
                     class="section-heading text-center mx-auto wow fadeInUp margin-b-30 animated">
                    <h3>
                        <a href="{{route('courses.show',['area'=>$area->id,'course'=>$course->id,'contenido'=>'videos'])}}">
                            <img class="icon_link" src="{{asset('/svg/arrow-left.svg')}}" alt="left_arrow">
                        </a>
                        {{$video->name}}
                    </h3>
                </div>
            </div>
            <div class="col-12">
                <video-component :video-uri="'{{$video->uri}}'"></video-component>
            </div>
            <div class="col-12 mt-1">
                <h4>{{__('Additional information')}}</h4>
                <p>
                    {!! $video->description !!}
                </p>
            </div>
        </div>
    </div>
@endsection
