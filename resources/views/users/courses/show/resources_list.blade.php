<div class="row" align="center">
    @forelse($resources as $resource)
        <div class="col-lg-4 col-md-6 col-sm-12 mt-3">
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">{{$resource->name}}</h5>
                    <a href="{{route('resources.player',['area'=>$area->id,'resource'=>$resource->id])}}"
                       class="card-link ">
                        <div class="media_linkable" align="center">
                            {{__('Read')}}
                        </div>
                    </a>
                </div>
            </div>
        </div>
    @empty
        @include('layouts.empty_content_indicator')
    @endforelse
</div>
