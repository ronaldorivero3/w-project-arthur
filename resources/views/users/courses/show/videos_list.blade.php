<div class="row" align="center">
    @forelse($videos as $video)
        <div class="col-lg-4 col-md-6 col-sm-12 mt-3">
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">{{$video->name}}</h5>
                    <a href="{{route('videos.player',['area'=>$area->id,'video'=>$video->id])}}" class="card-link ">
                        <div class="media_linkable" align="center">
                            {{__('Watch')}}
                        </div>
                    </a>
                </div>
            </div>
        </div>
    @empty
        @include('layouts.empty_content_indicator')
    @endforelse
</div>
