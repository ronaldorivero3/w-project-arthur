<a class="btn btn-warning" id="enable_users_btn"><i class="voyager-lightbulb"></i>
    <span>{{ __('Enable users') }}</span></a>

{{-- Enable users modal --}}
<div class="modal modal-warning fade" tabindex="-1" id="enable_users_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <i class="voyager-lightbulb"></i> {{ __('Are you sure to enable all users?') }}
                    <span></span> <span></span>
                </h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <form action="{{ route('users.enable') }}" method="POST">
                    {{ method_field("PUT") }}
                    {{ csrf_field() }}
                    <input type="submit" class="btn btn-warning pull-right delete-confirm"
                           value="{{__('Yes, enable all users')}}">
                </form>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                    {{ __('voyager::generic.cancel') }}
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
