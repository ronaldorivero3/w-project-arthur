<div class="row">
    @forelse($areas as $area)
        <div class="col-12 col-lg-6">
            <div class="single-top-popular-course d-flex align-items-center flex-wrap mb-30 wow fadeInUp"
                 data-wow-delay="400ms"
                 style="visibility: visible; animation-delay: 400ms; animation-name: fadeInUp;">
                <div class="popular-course-content">
                    <h5>{{$area->name}}</h5>
                    <p>{{$area->description}}</p>
                    <a href="{{route('areas.show',$area->id)}}" class="btn academy-btn btn-sm">
                        <span style="font-weight: bold; color:white;">
                            {{__('See More')}}
                        </span>
                    </a>
                </div>
                <div class="popular-course-thumb bg-img"
                     style="background-image: url('{{ Voyager::image( $area->banner) }}');">
                </div>
            </div>
        </div>
    @empty
        @include('layouts.empty_content_indicator')
    @endforelse
</div>
