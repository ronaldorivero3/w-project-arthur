<a class="btn btn-warning" id="disable_users_btn"><i class="voyager-power"></i>
    <span>{{ __('Disable users') }}</span></a>

{{-- Disable users modal --}}
<div class="modal modal-warning fade" tabindex="-1" id="disable_users_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <i class="voyager-power"></i> {{ __('Are you sure to disable all users?') }}
                    <span></span> <span></span>
                </h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <form action="{{ route('users.disable') }}" method="POST">
                    {{ method_field("PUT") }}
                    {{ csrf_field() }}
                    <input type="submit" class="btn btn-warning pull-right delete-confirm"
                           value="{{__('Yes, disable all users')}}">
                </form>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                    {{ __('voyager::generic.cancel') }}
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
