@extends('layouts.app')
@section('title')
    {{__('Home')}}
@endsection
@section('section_title')
    {{__('Welcome')}}
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div data-wow-delay="300ms"
                     class="section-heading text-center mx-auto wow fadeInUp margin-b-30 animated">
                    <h3>{{__('Your areas')}}</h3>
                    <span>
                    @if($areas->isNotEmpty())
                            {{$areas->count()}}
                            @if($areas->count()>1)
                                {{__('Areas')}}
                            @else
                                {{__('Area')}}
                            @endif
                        @endif
                </span>
                </div>
            </div>
        </div>
        @include('users.partials.areas_list')
    </div>
@endsection
