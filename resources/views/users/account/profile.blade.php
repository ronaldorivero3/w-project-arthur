@extends('layouts.app')
@section('title')
    {{__('Account')}}
@endsection
@section('section_title')
    {{__('Account')}}
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                @include('layouts.alerts')
            </div>
            <div class="col-12">
                <div data-wow-delay="300ms"
                     class="section-heading text-center mx-auto wow fadeInUp margin-b-30 animated">
                    <div class="row">
                        <div class="col-11">
                            <div class="section-heading text-center mx-auto wow fadeInUp" data-wow-delay="300ms"
                                 style="visibility: visible; animation-delay: 300ms; animation-name: fadeInUp;">
                                <h3>{{$user->name}}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="single-teachers-area text-center mb-100 wow fadeInUp" data-wow-delay="400ms"
                             style="visibility: visible; animation-delay: 400ms; animation-name: fadeInUp;">
                            <div class="teachers-thumbnail">
                                <img width="500" height="568" src="{{Voyager::image($user->avatar)}}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <form method="POST" enctype="multipart/form-data" action="{{route('account.update')}}">
                                @csrf
                                @method('put')
                                <div class="form-group row">
                                    <div class="teachers-info">
                                        <h5>
                                            <label for="avatar">
                                                {{__('Avatar')}}
                                            </label>
                                        </h5>
                                    </div>
                                    <div class="custom-file">
                                        <input type="file"
                                               class="custom-file-input{{ $errors->has('avatar') ? ' is-invalid' : '' }}"
                                               id="avatar" name="avatar" lang="es"
                                               accept="image/*">
                                        <label class="custom-file-label" for="avatar">{{__('Choose a image')}}</label>
                                        @if ($errors->has('avatar'))
                                            <div class="invalid-feedback text-left">
                                                {{$errors->first('avatar')}}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="teachers-info">
                                        <h5>
                                            <label for="name">
                                                {{__('Name')}}
                                            </label>
                                        </h5>
                                    </div>
                                    <input type="text"
                                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           id="name" name="name" value="{{$user->name}}">
                                    @if ($errors->has('name'))
                                        <div class="invalid-feedback text-left">
                                            {{$errors->first('name')}}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group row">
                                    <div class="teachers-info">
                                        <h5>
                                            <label for="ci">
                                                {{__('Credential information')}}
                                            </label>
                                        </h5>
                                    </div>
                                    <input type="text"
                                           class="form-control{{ $errors->has('ci') ? ' is-invalid' : '' }}"
                                           id="ci" name="ci" value="{{$user->ci}}">
                                    @if ($errors->has('ci'))
                                        <div class="invalid-feedback text-left">
                                            {{$errors->first('ci')}}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group row">
                                    <div class="teachers-info">
                                        <h5>
                                            <label for="email">
                                                {{__('E-Mail Address')}}
                                            </label>
                                        </h5>
                                    </div>
                                    <input type="email"
                                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           id="email" name="email"
                                           value="{{$user->email}}">
                                    @if ($errors->has('email'))
                                        <div class="invalid-feedback text-left">
                                            {{$errors->first('email')}}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group row">
                                    <div class="teachers-info">
                                        <h5>
                                            <label for="phone">
                                                {{__('Phone')}}
                                            </label>
                                        </h5>
                                    </div>
                                    <input type="text"
                                           class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                                           id="phone" name="phone" value="{{$user->phone}}">
                                    @if ($errors->has('phone'))
                                        <div class="invalid-feedback text-left">
                                            {{$errors->first('phone')}}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group row">
                                    <div class="teachers-info">
                                        <h5>
                                            <label for="direction">
                                                {{__('Direction')}}
                                            </label>
                                        </h5>
                                    </div>
                                    <input type="text"
                                           class="form-control{{ $errors->has('direction') ? ' is-invalid' : '' }}"
                                           id="direction" name="direction" value="{{$user->direction}}">
                                    @if ($errors->has('direction'))
                                        <div class="invalid-feedback text-left">
                                            {{$errors->first('direction')}}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group row">
                                    <div class="teachers-info">
                                        <h5>
                                            <label for="password">{{__('Password')}}</label>
                                        </h5>
                                    </div>
                                    <input type="password"
                                           class="form-control{{ $errors->has('direction') ? ' is-invalid' : '' }}"
                                           id="password" aria-describedby="passwordHelp"
                                           placeholder="{{__('Password')}}">
                                    @if ($errors->has('password'))
                                        <div class="invalid-feedback text-left">
                                            {{$errors->first('password')}}
                                        </div>
                                    @endif
                                    <small id="passwordHelp"
                                           class="form-text text-muted">{{__('Let in blank to conserve the last password')}}</small>
                                </div>
                                <button type="submit" class="btn academy-btn" style="color:white;">
                                    {{__('Saves')}}
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $('.custom-file-input').on('change', function () {
            let fileName = $(this).val().split('\\').pop();
            $(this).next('.custom-file-label').addClass("selected").html(fileName);
        });
    </script>
@endpush
