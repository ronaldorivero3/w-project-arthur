@extends('layouts.app')
@section('title')
    {{__('Simulator History')}}
@endsection
@section('section_title')
    {{__('Simulator History')}}
@endsection
@section('content')
    <div class="container">
        @if ($simulator!=null)
            <simulator :simulator-data='@json($simulator)'></simulator>
        @else
            <div class="row">
                <div class="col-12" align="center">
                    <h3>{{__('There is not information available')}}</h3>
                    <a href="{{route('simulator_histories.index')}}" class="btn academy-btn btn-4 m-2" role="button">
                        <span style="font-weight:bold">
                            {{__('Go back')}}
                        </span>
                    </a>
                </div>
            </div>
        @endif
    </div>
@endsection
