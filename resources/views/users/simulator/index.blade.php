@extends('layouts.app')
@section('title')
{{__('Simulador')}}
@endsection
@section('section_title')
{{__('Simulator')}}
@endsection
@section('content')
<div class="container">
    <simulator :simulator-data='@json($simulator)'></simulator>
</div>
@endsection