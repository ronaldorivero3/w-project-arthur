@extends('layouts.app')
@section('title')
    {{__('Simulator histories')}}
@endsection
@section('section_title')
    {{__('Simulator histories')}}
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div data-wow-delay="300ms"
                     class="section-heading text-center mx-auto wow fadeInUp margin-b-30 animated">
                    <h3>
                        <a href="{{route('inicio')}}">
                            <img class="icon_link" src="{{asset('/svg/arrow-left.svg')}}" alt="left_arrow">
                        </a>
                        {{__('You have done').' '.$simulatorHistories->count().' '.__('simulator tests')}}
                    </h3>
                </div>
            </div>
        </div>

        <div class="row" align="center">
            @include('users.simulator.histories_list')
            <div class="col-12 mt-2">
                {{$simulatorHistories->links()}}
            </div>
        </div>
    </div>
@endsection
