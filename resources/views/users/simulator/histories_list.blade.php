@forelse($simulatorHistories as $simulatorHistory)
    <div class="col-lg-4 col-md-6 col-sm-12 mt-3">
        <div class="card" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title" align="center">{{__('Area').': '.$simulatorHistory->area->name}}</h5>
                <h6 align="center">
                    {{$simulatorHistory->created_at}}
                </h6>
                <h6 align="center">{{__('Total score').': '.$simulatorHistory->score.'/40' }}</h6>
                <a href="{{route('simulator_histories.show',$simulatorHistory->id)}}" class="card-link ">
                    <div class="media_linkable" align="center">
                        {{__('See')}}
                    </div>
                </a>
            </div>
        </div>
    </div>

@empty
    @include('layouts.empty_content_indicator')
@endforelse
