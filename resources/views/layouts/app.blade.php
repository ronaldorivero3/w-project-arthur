<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <script>
        window.Laravel = {csrfToken: '{{ csrf_token() }}'}
    </script>
    <title>{{config('app.name')}} - @yield('title')</title>
    <link rel="icon" href="{{asset('img/core-img/favicon.ico')}}">
    @include('layouts.styles')
    @stack('styles')
</head>

<body>
    <div id="preloader">
        <i class="circle-preloader"></i>
    </div>
    @include('layouts.header')
    <div id="app">
        <main class="academy-courses-area section-padding-100-0">
            @yield('content')
        </main>
        @include('layouts.footer')
    </div>
    @include('layouts.scripts')
    @stack('scripts')
</body>

</html>