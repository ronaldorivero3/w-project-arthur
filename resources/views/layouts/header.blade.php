<header class="header-area">
    <div class="top-header">
        <div class="container h-100">
            <div class="row h-100">
                <div class="col-12 h-100">
                    <div class="header-content h-100 d-flex align-items-center justify-content-between">
                        <div class="academy-logo">
                            <a href="/">
                                <img width="153" height="27" src="{{Voyager::image(setting('site.logo'))}}" alt="">
                            </a>
                        </div>
                        @auth
                            <div class="logout">
                                <a href="{{ route('account.profile') }}"
                                   style="color:#61BA6D"> {{auth()->user()->name}}</a>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <img src="{{asset('img/sesion-img/logout.png')}}" alt="logout.png">
                                </a>
                            </div>
                        @endauth

                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--Navigation menu--}}
    <div class="academy-main-menu">
        <div class="classy-nav-container breakpoint-off">
            <div class="container">
                <nav class="classy-navbar justify-content-between" id="academyNav">
                    <div class="classy-navbar-toggler">
                        <span class="navbarToggler"><span></span><span></span><span></span></span>
                    </div>
                    <div class="classy-menu">
                        <div class="classycloseIcon">
                            <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                        </div>
                        <div class="classynav">
                            @auth
                                {{menu('student_menu','layouts.menu')}}
                            @endauth
                            @guest
                                {{menu('guest','layouts.menu')}}
                            @endguest
                        </div>
                    </div>
                    <div class="calling-info">
                        <div class="call-center">
                            <a href="/"><i class="icon-house"></i>
                                <span>@yield('section_title')</span>
                            </a>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <form id="logout-form" action="{{ route('logout') }}" method="POST"
          style="display: none;">
        @csrf
    </form>
</header>
