<footer class="footer-area mt-100">
    <div class="main-footer-area section-padding-100-0">
        <div class="container">
            <div class="row">

                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="footer-widget mb-100">
                        <div class="widget-title">
                            <a href="#">
                                <img width="153" height="27" src="{{Voyager::image(setting('site.logo'))}}" alt="">
                            </a>
                        </div>
                        <p>
                            {!! setting('site.description') !!}
                        </p>
                        <div class="footer-social-info">
                            <a target="__blank" href="{{setting('media.facebook')}}"><i class="fa fa-facebook"></i></a>
                            <a target="__blank" href="{{setting('media.twitter')}}"><i class="fa fa-twitter"></i></a>
                            <a target="__blank" href="{{setting('media.instagram')}}"><i
                                    class="fa fa-instagram"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="footer-widget mb-100">
                        <div class="widget-title">
                            <h6>{{__('Useful Links')}}</h6>
                        </div>
                        <nav>
                            <ul class="useful-links">
                                <li><a href="/">{{__('Home')}}</a></li>
                                <li><a href="{{route('site.policies')}}">{{__('Privacy policies')}}</a></li>
                                <li><a href="{{route('site.conditions')}}">{{__('Terms and conditions')}}</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="footer-widget mb-100">
                        <div class="widget-title">
                            <h6>{{__('Contact')}}</h6>
                        </div>
                        <div class="single-contact d-flex mb-30">
                            <i class="icon-placeholder"></i>
                            <p>{!! setting('site.direction') !!}</p>
                        </div>
                        <div class="single-contact d-flex mb-30">
                            <i class="icon-telephone-1"></i>
                            <p>
                                Principal: {!! setting('site.phone_primary') !!}
                                <br>
                                Oficina: {!! setting('site.phone_secondary') !!}
                            </p>
                        </div>
                        <div class="single-contact d-flex">
                            <i class="icon-contract"></i>
                            <p>
                                {!! setting('site.email') !!}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-footer-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <p>
                        Copyright &copy;2020
                        All rights reserved | This project was develop by
						<a href="https://www.instagram.com/ronaldorivero3" target="_blank">Ronaldo Rivero Gonzales</a>|
                        <a href="https://www.instagram.com/paulgrimaldo.dev" target="_blank">Paul Grimaldo</a>|
                        <a href="https://www.instagram.com/fabiocortezm" target="_blank">Fabio Cortez</a>|
                        <a href="https://www.instagram.com/joaquin.3108" target="_blank">Joaquin Blanco</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
