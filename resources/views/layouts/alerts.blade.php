<div class="col-12">
    @if(session()->has('alert-type'))
        @switch(session()->get('alert-type'))
            @case('success')
            <div class="alert alert-success" role="alert">
                {{session()->get('message')}}
            </div>
            @break
            @case('error')
            <div class="alert alert-danger" role="alert">
                {{session()->get('message')}}
            </div>
            @break
            @case('warning')
            <div class="alert alert-warning" role="alert">
                {{session()->get('message')}}
            </div>
            @break
            @case('info')
            <div class="alert alert-info" role="alert">
                {{session()->get('message')}}
            </div>
            @break
        @endswitch
    @endif
</div>