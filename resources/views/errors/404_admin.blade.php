@extends('voyager::master')

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('Error 404'))

@section('page_header')
<h1 class="page-title">
    <i class="voyager-x"></i>
    {{ __('Error 404') }}
</h1>
@include('voyager::multilingual.language-selector')
@stop

@section('content')
<div class="page-content edit-add container-fluid">
    <div class="row">
        <div class="col-md-12 center-block text-center">
            <div class="panel panel-bordered">
                <h3>
                    {{__("Can't find the resource you are looking for")}}
                </h3>
                <a href="/admin" class="btn btn-danger">
                    <i class="voyager-angle-left"></i>
                    <span>
                        {{__('Go back')}}
                    </span>
                </a>
            </div>
        </div>
    </div>
</div>
@stop