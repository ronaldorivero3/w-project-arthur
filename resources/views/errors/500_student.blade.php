@extends('layouts.app')
@section('title')
    {{__('Error 500')}}
@endsection
@section('section_title')
    {{__('Error 500')}}
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12" align="center">
                <h1>Opsss....</h1>
                <h4>
                    {{__('An unexpected error occurred on the server, your request cannot continue')}}
                </h4>
                <a href="/"
                   class="btn academy-btn btn-2 m-2 pt-3" style="color:white;">
                    {{__('Go back')}}
                </a>
            </div>
        </div>
    </div>
@stop
