<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="disabledAccount" id="disabledAccountModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="loginModalLabel">{{ __('Disabled account') }}</h4>
                <img width="153" height="27" src="{{Voyager::image(setting('site.logo'))}}" alt="logo">
            </div>
            <div class="modal-body">
                <h5>{{__('Your account is not enabled to use this system')}}</h5>
                <p>{{__('If you think this was a mistake please contact with the administrator')}}</p>
            </div>
            <div class="modal-footer">
                <a target="_blank"
                   href="https://api.whatsapp.com/send?phone=591{{setting('site.phone_primary')}}&text=Hola%20mi%20cuenta%20en%20{{config('app.name')}}%20esta%20desactivada%20por%20error"
                   class="btn academy-btn btn-2 m-2" style="color:white;">
                    {{__('Contact')}}
                </a>
            </div>
        </div>
    </div>
</div>
