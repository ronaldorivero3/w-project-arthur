<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8"/><!-- /Added by HTTrack -->
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{config('app.name')}}</title>
    <link rel="icon" href="{{asset('img/core-img/favicon.ico')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>
<body>

<div id="preloader">
    <i class="circle-preloader"></i>
</div>
<header class="header-area">
    <div class="top-header">
        <div class="container h-100">
            <div class="row h-100">
                <div class="col-12 h-100">
                    <div class="header-content h-100 d-flex align-items-center justify-content-between">
                        <div class="academy-logo">
                            <a href="\">
                                <img width="153" height="27" src="{{Voyager::image(setting('site.logo'))}}" alt="">
                            </a>
                        </div>
                        <!--Aqui esta el login-->
                        <div>
                            <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
                                <div class="container">
                                    <ul class="navbar-nav ml-auto">
                                        <li class="nav-item">
                                            <a href="#" data-toggle="modal"
                                               data-target="#loginModal">{{ __('Log in') }}</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                        <!--Aqui acaba el login-->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="academy-main-menu">
        <div class="classy-nav-container breakpoint-off">
            <div class="container">

                <nav class="classy-navbar justify-content-between" id="academyNav">

                    <div class="classy-navbar-toggler">
                        <span class="navbarToggler"><span></span><span></span><span></span></span>
                    </div>

                    <div class="classy-menu">

                        <div class="classycloseIcon">
                            <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                        </div>

                        <div class="classynav">
                            <ul>
                                <li><a href="{{ route('inicio') }}">{{__('Home')}}</a></li>
                                <li><a href="{{ route('inicio') }}/#information">{{__('About us')}}</a></li>
                                <li><a href="{{ route('inicio') }}/#class">{{__('Courses')}}</a></li>
                                <li><a href="{{ route('inicio') }}/#contacts">{{__('Contact')}}</a></li>
                            </ul>
                        </div>

                    </div>

                    <div class="calling-info">
                        <div class="call-center">
                            <a href="https://api.whatsapp.com/send?phone=591{{setting('site.phone_primary')}}&text=Proyecto%20Arthur"><i
                                    class="icon-telephone-2"></i>
                                <span>(+591) {!! setting('site.phone_primary') !!}</span></a>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>
<!--Aqui esta el modal del login-->
@include('auth.modal_login')
<div id="login_errors" data-errors=@json($errors->all())></div>
<section class="hero-area">
    <div class="hero-slides owl-carousel">

        <div class="single-hero-slide bg-img" style="background-image: url('{{asset('img/bg-img/bg_1.png')}}');">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12">
                        <div class="hero-slides-content">
                            <h4 data-animation="fadeInUp" data-delay="100ms">{{__('All the courses you need')}}</h4>
                            <h2 data-animation="fadeInUp" data-delay="400ms">{{__('Welcome to')}}
                                <br>{{__('online learning')}}</h2>
                            <a href="{{ route('inicio') }}/#information" class="btn academy-btn"
                               data-animation="fadeInUp" data-delay="700ms">{{__('Read more')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="single-hero-slide bg-img" style="background-image: url('{{asset('img/bg-img/bg_2.png')}}');">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12">
                        <div class="hero-slides-content">
                            <h4 data-animation="fadeInUp" data-delay="100ms">{{__('All the courses you need')}}</h4>
                            <h2 data-animation="fadeInUp" data-delay="400ms">{{__('Welcome to')}}
                                <br>{{__('online learning')}}</h2>
                            <a href="{{ route('inicio') }}/#information" class="btn academy-btn"
                               data-animation="fadeInUp" data-delay="700ms">{{__('Read more')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="single-hero-slide bg-img" style="background-image: url('{{asset('img/bg-img/bg_3.png')}}');">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12">
                        <div class="hero-slides-content">
                            <h4 data-animation="fadeInUp" data-delay="100ms">{{__('All the courses you need')}}</h4>
                            <h2 data-animation="fadeInUp" data-delay="400ms">{{__('Welcome to')}}
                                <br>{{__('online learning')}}</h2>
                            <a href="{{ route('inicio') }}/#information" class="btn academy-btn"
                               data-animation="fadeInUp" data-delay="700ms">{{__('Read more')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="top-features-area wow fadeInUp" data-wow-delay="300ms">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="features-content">
                    <div class="row no-gutters">

                        <div class="col-12 col-md-4">
                            <div class="single-top-features d-flex align-items-center justify-content-center">
                                <i class="icon-agenda-1"></i>
                                <h5>{{__('Recorded Courses')}}</h5>
                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="single-top-features d-flex align-items-center justify-content-center">
                                <i class="icon-assistance"></i>
                                <h5>{{__('Amazing Teachers')}}</h5>
                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="single-top-features d-flex align-items-center justify-content-center">
                                <i class="icon-telephone-3"></i>
                                <h5>{{__('Instantly Help')}}</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="academy-courses-area section-padding-100-0" id="information">
    <div class="container">
        <div class="row">

            <div class="col-12 col-sm-6 col-lg-4">
                <div class="single-course-area d-flex align-items-center mb-100 wow fadeInUp" data-wow-delay="300ms">
                    <div class="course-icon">
                        <i class="icon-id-card"></i>
                    </div>
                    <div class="course-content">
                        <h4>{!! setting('site.us_title_1') !!}</h4>
                        <p>{!! setting('site.us_desc_1') !!}</p>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-6 col-lg-4">
                <div class="single-course-area d-flex align-items-center mb-100 wow fadeInUp" data-wow-delay="400ms">
                    <div class="course-icon">
                        <i class="icon-worldwide"></i>
                    </div>
                    <div class="course-content">
                        <h4>{!! setting('site.us_title_2') !!}</h4>
                        <p>{!! setting('site.us_desc_2') !!}</p>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-6 col-lg-4">
                <div class="single-course-area d-flex align-items-center mb-100 wow fadeInUp" data-wow-delay="800ms">
                    <div class="course-icon">
                        <i class="icon-message"></i>
                    </div>
                    <div class="course-content">
                        <h4>{!! setting('site.us_title_3') !!}</h4>
                        <p>{!! setting('site.us_desc_3') !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="top-popular-courses-area section-padding-100-70" id='class'>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-heading text-center mx-auto wow fadeInUp" data-wow-delay="300ms">
                    <span>{{__('Check it out')}}</span>
                    <h3>{{__('Ours areas more populars')}}</h3>
                </div>
            </div>
        </div>

        <div class="row">
            @forelse($areas as $area)
                <div class="col-12 col-lg-6">
                    <div class="single-top-popular-course d-flex align-items-center flex-wrap mb-30 wow fadeInUp"
                         data-wow-delay="400ms">
                        <div class="popular-course-content">
                            <h5>{{$area->name}}</h5>
                            <span>{{$area->formatted_date}}</span>
                            <div class="course-ratings">
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star-o" aria-hidden="true"></i>
                            </div>
                            <p>{{$area->description}} </p>
                        </div>
                        <div class="popular-course-thumb bg-img"
                             style="background-image: url('{{ Voyager::image( $area->banner) }}');">
                        </div>
                    </div>
                </div>
            @empty
                <h3>{{__('There is not information available')}}</h3>
            @endforelse
        </div>
        <div class="row">
            <div class="col-12">
                <div class="section-heading text-center mx-auto wow fadeInUp" data-wow-delay="300ms">
                <span>{{__('Exits a total of ')}}
                    {{$number_areas}}
                    {{__('areas available')}}
                </span>
                </div>
            </div>
        </div>
    </div>
</div>

<section id='contacts'>
    @include('layouts.footer')
</section>


<script src="{{asset('js/jquery/jquery-2.2.4.min.js')}}" type="text/javascript"></script>

<script src="{{asset('js/bootstrap/popper.min.js')}}" type="a172c6bc896375b4eab0897d-text/javascript"></script>

<script src="{{asset('js/bootstrap/bootstrap.min.js')}}" type="text/javascript"></script>

@if($errors->isNotEmpty())
    <script type="text/javascript">
        $(document).ready(function () {
            $('#loginModal').modal('show');
        });
    </script>
@endif

@if(session('disabled_account'))
    @include('auth.modal_disabled_account')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#disabledAccountModal').modal('show');
        });
    </script>
@endif

<script src="{{asset('js/plugins/plugins.js')}}" type="a172c6bc896375b4eab0897d-text/javascript"></script>

<script src="{{asset('js/active.js')}}" type="a172c6bc896375b4eab0897d-text/javascript"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"
        type="a172c6bc896375b4eab0897d-text/javascript"></script>
<script type="javascript">
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());
    gtag('config', 'UA-23581568-13');
</script>
<script src="{{asset('js/rocket-loader.min.js')}}"
        data-cf-settings="a172c6bc896375b4eab0897d-|49" defer=""></script>
</body>
</html>
