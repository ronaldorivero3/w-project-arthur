@extends('layouts.app')
@section('title')
{{__('Privacy policies')}}
@endsection
@section('section_title')
{{__('Privacy policies')}}
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            {!! setting('site.privacy_policies') !!}
        </div>
    </div>
</div>
@stop