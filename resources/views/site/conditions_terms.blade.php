@extends('layouts.app')
@section('title')
{{__('Terms and conditions')}}
@endsection
@section('section_title')
{{__('Terms and conditions')}}
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            {!! setting('site.conditions_terms') !!}
        </div>
    </div>
</div>
@stop