<?php

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
/**
 *ENABLE AND DISABLE USERS
 */
Route::put('/users/disable', 'Admin\UserController@disable')->name('users.disable');
Route::put('/users/enable', 'Admin\UserController@enable')->name('users.enable');
