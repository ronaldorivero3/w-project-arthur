<?php

Auth::routes(['register' => false]);
Route::get('/', 'Users\DashboardController@index')->name('inicio');
Route::get('/politicas-de-privacidad', 'Admin\SiteController@getPrivacyPolicies')->name('site.policies');
Route::get('/terminos-y-condiciones', 'Admin\SiteController@getConditionsTerms')->name('site.conditions');
/**
 *ACADEMIC AREA
 */
Route::get('/areas/{area}', 'Users\AreaController@show')->name('areas.show');
Route::get('/cursos/{area}/{course}/{contenido?}', 'Users\CourseController@show')->name('courses.show');

Route::get('/simulador/{area}', 'Users\SimulatorController@show')->name('simulator.show');
Route::post('/finish-simulator', 'Users\SimulatorController@store')->name('simulator.store');
Route::post('/cancel-simulator', 'Users\SimulatorController@cancel')->name('simulator.cancel');
/**
 *USER ACCOUNT
 */
Route::get('/cuenta', 'Users\AccountController@profile')->name('account.profile');
Route::put('/cuenta', 'Users\AccountController@update')->name('account.update');
/**
 * LIST VIDEO AND RESOURCE
 */
Route::get('/video-reproductor/{area}/{video}', 'Users\VideoController@showVideo')->name('videos.player');
Route::get('/recurso-reproductor/{area}/{resource}', 'Users\ResourceController@showResource')->name('resources.player');
/**
 *STREAM VIDEO AND RESOURCE
 */
Route::get('/videos/{video}', 'Users\VideoController@getVideo')->name('videos.show');
Route::get('/resources/{resource}', 'Users\ResourceController@getResource')->name('resources.show');
/**
 * USER HISTORIES
 */
Route::get('/historial', 'Users\SimulatorController@index')->name('simulator_histories.index');
Route::get('/historial/{simulatorHistory}', 'Users\SimulatorController@showDetail')->name('simulator_histories.show');
