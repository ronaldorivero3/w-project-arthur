<?php
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

// Inicio
Breadcrumbs::for('home', function ($trail) {
    $trail->push('Inicio', route('inicio'));
});

//Inicio>Area
Breadcrumbs::for('area', function ($trail, $area) {
    $trail->parent('home');
    $trail->push($area->name, route('areas.show', $area));
});

//Inicio>Area>Course
Breadcrumbs::for('course', function ($trail, $course,$area) {
    $trail->parent('area',$area);
    $trail->push($course->name, route('courses.show', $area));
});

